package okhttp3.compare;

import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import mockwebserver3.RecordedRequest;
import okhttp3.*;
import okhttp3.internal.platform.Platform;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.IOException;

public class OkHttpClientTest {
    public final PlatformRule platform = new PlatformRule(null,null);
    public static void main(String[] args) {
        OkHttpClientTest test=new OkHttpClientTest();
        test.get(new MockWebServer());
    }
    public final void get( MockWebServer server) {
        server.enqueue((new MockResponse()).setBody("hello, OkHttp"));
        OkHttpClient client = new OkHttpClient();
        Request request = (new Request.Builder()).url(server.url("/")).header("Accept", "text/plain").build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(response.code()).isEqualTo(200);
        ResponseBody var10000 = response.body();
        try {
            Assertions.assertThat(var10000.string()).isEqualTo("hello, OkHttp");
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        RecordedRequest recorded = null;
        try {
            recorded = server.takeRequest();
        } catch (InterruptedException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(recorded.getHeader("Accept")).isEqualTo("text/plain");
        Assertions.assertThat(recorded.getHeader("Accept-Encoding")).isEqualTo("gzip");
        Assertions.assertThat(recorded.getHeader("Connection")).isEqualTo("Keep-Alive");
        Assertions.assertThat(recorded.getHeader("User-Agent")).matches((CharSequence)"okhttp/.*");
    }
}
