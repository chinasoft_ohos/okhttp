package okhttp3.compare;

import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import mockwebserver3.RecordedRequest;
import org.assertj.core.api.Assertions;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.ContentResponse;
import org.eclipse.jetty.client.api.Request;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class JettyHttpClientTest {
    private final HttpClient client = new HttpClient();

//    @BeforeEach
    public final void setUp() {
        try {
            this.client.start();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }

//    @AfterEach
    public final void tearDown() {
        try {
            this.client.stop();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
    }
    public static void main(String[] args) {
        JettyHttpClientTest test=new JettyHttpClientTest();
        test.setUp();
        test.get(new MockWebServer());
    }
    public final void get( MockWebServer server) {
        server.enqueue((new MockResponse()).setBody("hello, Jetty HTTP Client"));
        Request request = this.client.newRequest(server.url("/").uri()).header("Accept", "text/plain");
        ContentResponse response = null;
        try {
            response = request.send();
        } catch (InterruptedException e) {
            e.fillInStackTrace();
        } catch (TimeoutException e) {
            e.fillInStackTrace();
        } catch (ExecutionException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(response.getStatus()).isEqualTo(200);
        Assertions.assertThat(response.getContentAsString()).isEqualTo("hello, Jetty HTTP Client");
        RecordedRequest recorded = null;
        try {
            recorded = server.takeRequest();
        } catch (InterruptedException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(recorded.getHeader("Accept")).isEqualTo("text/plain");
        Assertions.assertThat(recorded.getHeader("Accept-Encoding")).isEqualTo("gzip");
        Assertions.assertThat(recorded.getHeader("Connection")).isNull();
        Assertions.assertThat(recorded.getHeader("User-Agent")).matches((CharSequence)"Jetty/.*");
    }
}
