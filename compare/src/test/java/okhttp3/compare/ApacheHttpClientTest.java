package okhttp3.compare;

import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import mockwebserver3.RecordedRequest;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.assertj.core.api.AbstractStringAssert;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.Closeable;
import java.io.IOException;

public class ApacheHttpClientTest {
    private final CloseableHttpClient httpClient = HttpClients.createDefault();

    //    @AfterEach
    public final void tearDown() {
        try {
            this.httpClient.close();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
    }

    public static void main(String[] args) {
        ApacheHttpClientTest test=new ApacheHttpClientTest();
        test.get(new MockWebServer());
    }

//    @Test
    public final void get(MockWebServer server) {
        server.enqueue((new MockResponse()).setBody("hello, Apache HttpClient 5.x"));
        HttpGet request = new HttpGet(server.url("/").uri());
        request.addHeader("Accept", "text/plain");
        Closeable var3 = null;
        try {
            var3 = (Closeable) this.httpClient.execute((ClassicHttpRequest) request);
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        boolean var4 = false;
        boolean var5 = false;
        Throwable var13 = (Throwable) null;

        try {
            CloseableHttpResponse response = (CloseableHttpResponse) var3;
            Assertions.assertThat(response.getCode()).isEqualTo(200);
            AbstractStringAssert var14 = Assertions.assertThat(EntityUtils.toString(response.getEntity())).isEqualTo("hello, Apache HttpClient 5.x");
        } catch (Throwable var10) {
            var13 = var10;
            try {
                throw var10;
            } catch (IOException e) {
                e.fillInStackTrace();
            } catch (ParseException e) {
                e.fillInStackTrace();
            }
        } finally {
        }

        RecordedRequest recorded = null;
        try {
            recorded = server.takeRequest();
        } catch (InterruptedException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(recorded.getHeader("Accept")).isEqualTo("text/plain");
        Assertions.assertThat(recorded.getHeader("Accept-Encoding")).isEqualTo("gzip, x-gzip, deflate");
        Assertions.assertThat(recorded.getHeader("Connection")).isEqualTo("keep-alive");
        Assertions.assertThat(recorded.getHeader("User-Agent")).startsWith((CharSequence) "Apache-HttpClient/5.0");
    }
}
