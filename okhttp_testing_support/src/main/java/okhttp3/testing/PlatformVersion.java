package okhttp3.testing;

public class PlatformVersion {
//    private static final Lazy majorVersion$delegate;
    public static final PlatformVersion INSTANCE;

    public final int getMajorVersion() {
//        Lazy var1 = majorVersion$delegate;
        Object var3 = null;
        boolean var4 = false;
        return 0;
    }

    public final String getJvmSpecVersion() {
        String var10000 = System.getProperty("java.specification.version", "unknown");
        return var10000;
    }

    private PlatformVersion() {
    }

    static {
        PlatformVersion var0 = new PlatformVersion();
        INSTANCE = var0;
//        majorVersion$delegate = LazyKt.lazy((Function0)null.INSTANCE);
    }
}
