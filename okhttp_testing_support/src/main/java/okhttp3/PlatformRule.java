package okhttp3;



import okhttp3.internal.platform.*;
//import okhttp3.internal.platform.Jdk8WithJettyBootPlatform;
//import okhttp3.internal.platform.Jdk9Platform;
import okhttp3.testing.PlatformVersion;
import org.conscrypt.Conscrypt;
import org.hamcrest.*;

import org.junit.Assume;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.extension.*;
import org.junit.rules.ExternalResource;

import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static okhttp3.internal.platform.Platform.get;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class PlatformRule extends ExternalResource {
    private String requiredPlatformName;
    private Platform platform;
    public String PROPERTY_NAME = "okhttp.platform";
    public String CONSCRYPT_PROPERTY = "conscrypt";
    public String JDK9_PROPERTY = "jdk9";
    public String JDK8_ALPN_PROPERTY = "jdk8alpn";
    public String JDK8_PROPERTY = "jdk8";
    public String OPENJSSE_PROPERTY = "openjsse";
    public PlatformRule.Companion Companion;

    protected void before() {
        if (this.requiredPlatformName != null) {
            Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.equalTo(this.requiredPlatformName));
        }

        if (this.platform != null) {
            Platform.resetForTests(this.platform);
        } else {
            Platform.resetForTests();
        }

    }

    protected void after() {
        if (this.platform != null) {
            Platform.resetForTests(this.platform);
        }

    }
    public void assumeOpenJSSE() {
        assumeTrue(getPlatformSystemProperty() == OPENJSSE_PROPERTY);
    }
    public final boolean isConscrypt() {
        return getPlatformSystemProperty().equals("conscrypt");
    }

    public final boolean isJdk9() {
        return getPlatformSystemProperty().equals( "jdk9");
    }

    public final boolean isJdk8() {
        return getPlatformSystemProperty().equals("jdk8");
    }

    public final boolean isJdk8Alpn() {
        return getPlatformSystemProperty().equals("jdk8alpn");
    }

    public final boolean hasHttp2Support() {
        return !this.isJdk8();
    }

    public final void assumeConscrypt() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.equalTo("conscrypt"));
    }

    public final void assumeJdk9() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.equalTo("jdk9"));
    }

    public final void assumeJdk8() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.equalTo("jdk8"));
    }

    public final void assumeJdk8Alpn() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.equalTo("jdk8alpn"));
    }

    public final void assumeHttp2Support() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.not("jdk8"));
    }

    public final void assumeNotConscrypt() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.not("conscrypt"));
    }

    public final void assumeNotJdk9() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.not("jdk9"));
    }

    public final void assumeNotJdk8() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.not("jdk8"));
    }

    public final void assumeNotJdk8Alpn() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.not("jdk8alpn"));
    }

    public final void assumeNotHttp2Support() {
        Assume.assumeThat(getPlatformSystemProperty(), CoreMatchers.equalTo("jdk8"));
    }

    public final void assumeJettyBootEnabled() {
        Assume.assumeTrue("ALPN Boot not enabled", isAlpnBootEnabled());
    }

    public final String getRequiredPlatformName() {
        return this.requiredPlatformName;
    }

    public final Platform getPlatform() {
        return this.platform;
    }

    public PlatformRule(String requiredPlatformName, Platform platform) {
        this.requiredPlatformName = requiredPlatformName;
        this.platform = platform;
    }


    public PlatformRule( String requiredPlatformName) {
        this(requiredPlatformName,  null);
    }



    public static final String getPlatformSystemProperty() {
        String property = System.getProperty("okhttp.platform");
        if (property == null) {
            Platform var2 = get();
            property = var2 instanceof ConscryptPlatform ? "conscrypt" : (var2 instanceof Jdk8WithJettyBootPlatform ? "conscrypt" : (var2 instanceof Jdk9Platform ? "jdk9" : "jdk8"));
        }

        return property;
    }


    public static final PlatformRule conscrypt() {
        return new PlatformRule("conscrypt",null);
    }


    public static final PlatformRule jdk9() {
        return jdk9();
    }


    public static final PlatformRule jdk8() {
        return jdk8();
    }


    public static final PlatformRule jdk8alpn() {
        return jdk8alpn();
    }

    public static final boolean isAlpnBootEnabled() {
        return isAlpnBootEnabled();
    }


    public static final class Companion {
        public final String getPlatformSystemProperty() {
            String property = System.getProperty("okhttp.platform");
            if (property == null) {
                Platform var2 = get();
                property = var2 instanceof ConscryptPlatform ? "conscrypt" : (var2 instanceof Jdk8WithJettyBootPlatform ? "conscrypt" : (var2 instanceof Jdk9Platform ? "jdk9" : "jdk8"));
            }

            return property;
        }

        {
            if (getPlatformSystemProperty().equals("conscrypt")) {
                Provider var10000 = Security.getProviders()[0];
                if (var10000.getName().equals("Conscrypt") ^ true) {
                    if (!Conscrypt.isAvailable()) {
                        System.err.println("Warning: Conscrypt not available");
                    }

                    Provider provider = Conscrypt.newProviderBuilder().provideTrustManager(true).build();
                    Security.insertProviderAt(provider, 1);
                }
            }

            if (getPlatformSystemProperty().equals("jdk8alpn")) {
                if (!isAlpnBootEnabled()) {
                    System.err.println("Warning: ALPN Boot not enabled");
                }
            } else if (getPlatformSystemProperty().equals("jdk8") && isAlpnBootEnabled()) {
                System.err.println("Warning: ALPN Boot enabled unintentionally");
            }
        }

        public final PlatformRule conscrypt() {
            return new PlatformRule("conscrypt", null);
        }

        public final PlatformRule jdk9() {
            return new PlatformRule("jdk9", null);
        }

        public final PlatformRule jdk8() {
            return new PlatformRule("jdk8", null);
        }

        public final PlatformRule jdk8alpn() {
            return new PlatformRule("jdk8alpn", null);
        }

        public final boolean isAlpnBootEnabled() {
            boolean var1;
            try {
                Class.forName("org.eclipse.jetty.alpn.ALPN", true, (ClassLoader) null);
                var1 = true;
            } catch (ClassNotFoundException var3) {
                var1 = false;
            }

            return var1;
        }

        private Companion() {
        }


    }
}
