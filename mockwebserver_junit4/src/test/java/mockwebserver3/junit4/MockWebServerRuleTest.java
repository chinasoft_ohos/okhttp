package mockwebserver3.junit4;

import org.assertj.core.api.AbstractBooleanAssert;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

public class MockWebServerRuleTest {
public static void main(String[] args){
    MockWebServerRuleTest test=new MockWebServerRuleTest();
    test.statementStartsAndStops();
}
public final void statementStartsAndStops() {
        final MockWebServerRule rule = new MockWebServerRule();
        final AtomicBoolean called = new AtomicBoolean();
        Statement var10000 = rule.apply((Statement) (new Statement() {
            public void evaluate() throws Throwable {
                called.set(true);
                rule.getServer().url("/").url().openConnection().connect();
            }
        }), Description.EMPTY);
        Statement statement = var10000;
        try {
            statement.evaluate();
        } catch (Throwable throwable) {
            throwable.fillInStackTrace();
        }
        AbstractBooleanAssert var6 = Assertions.assertThat(called.get());
        var6.isTrue();

        try {
            System.out.println(rule.getServer().url("/").url());
//            URL url=new URL("http://127.0.0.1:8080/");
            rule.getServer().url("/").url().openConnection().connect();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
//        Assert.fail();
    }
}
