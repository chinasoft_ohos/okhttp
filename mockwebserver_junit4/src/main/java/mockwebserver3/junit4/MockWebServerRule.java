package mockwebserver3.junit4;

import mockwebserver3.MockWebServer;
import org.junit.rules.ExternalResource;

import java.io.IOException;

public class MockWebServerRule extends ExternalResource {
    private final MockWebServer server = new MockWebServer();
//    private static final Logger logger = Logger.getLogger(MockWebServerRule.class.getName());
//    public static final MockWebServerRule.Companion Companion = new MockWebServerRule.Companion((DefaultConstructorMarker)null);

    public final MockWebServer getServer() {
        return this.server;
    }

    protected void before() {
        try {
            server.start();
        } catch (IOException var2) {
//            throw (Throwable)(new RuntimeException((Throwable)var2));
        }
    }

    protected void after() {
        try {
            this.server.shutdown();
        } catch (IOException var2) {
//            logger.log(Level.WARNING, "MockWebServer shutdown failed", (Throwable)var2);
        }

    }
}

