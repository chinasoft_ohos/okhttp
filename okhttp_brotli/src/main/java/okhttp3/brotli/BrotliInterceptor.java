package okhttp3.brotli;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.brotli.internal.BrotliKt;

import java.io.IOException;

public class BrotliInterceptor implements Interceptor {
    public static final BrotliInterceptor INSTANCE;

    public Response intercept(Chain chain) {
        Response response = null;
        try {
            if (chain.request().header("Accept-Encoding") == null) {
                Request request = chain.request().newBuilder().header("Accept-Encoding", "br,gzip").build();
                Response res = null;

                res = chain.proceed(request);

                response = BrotliKt.uncompress(res);
            } else {
                response = chain.proceed(chain.request());
            }
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        return response;
    }

    private BrotliInterceptor() {
    }

    static {
        BrotliInterceptor brotliInterceptor = new BrotliInterceptor();
        INSTANCE = brotliInterceptor;
    }
}
