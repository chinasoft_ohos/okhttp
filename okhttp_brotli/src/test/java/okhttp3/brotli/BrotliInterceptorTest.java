package okhttp3.brotli;

import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.brotli.internal.BrotliKt;
import okio.ByteString;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.IOException;

public class BrotliInterceptorTest {
    @Test
    public final void testUncompressBrotli() {
        String s = "1bce00009c05ceb9f028d14e416230f718960a537b0922d2f7b6adef56532c08dff44551516690131494db6021c7e3616c82c1bc2416abb919aaa06e8d30d82cc2981c2f5c900bfb8ee29d5c03deb1c0dacff80eabe82ba64ed250a497162006824684db917963ecebe041b352a3e62d629cc97b95cac24265b175171e5cb384cd0912aeb5b5dd9555f2dd1a9b20688201";
        Response response = this.response("https://httpbin.org/brotli", ByteString.decodeHex(s));
        Response uncompressed = BrotliKt.uncompress(response);
        ResponseBody var10000 = uncompressed.body();
        String responseString = null;
        try {
            responseString = var10000 != null ? var10000.string() : null;
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(responseString).contains(new CharSequence[]{(CharSequence)"\"brotli\": true,"});
        Assertions.assertThat(responseString).contains(new CharSequence[]{(CharSequence)"\"Accept-Encoding\": \"br\""});
    }

    @Test
    public final void testUncompressGzip() {
        String s = "1f8b0800968f215d02ff558ec10e82301044ef7c45b3e75269d0c478e340e4a426e007086c4a636c9bb65e24fcbb5b484c3cec61deccecee9c3106eaa39dc3114e2cfa377296d8848f117d20369324500d03ba98d766b0a3368a0ce83d4f55581b14696c88894f31ba5e1b61bdfa79f7803eaf149a35619f29b3db0b298abcbd54b7b6b97640c965bbfec238d9f4109ceb6edb01d66ba54d6247296441531e445970f627215bb22f1017320dd5000000";
        Response response = this.response("https://httpbin.org/gzip", ByteString.decodeHex(s));
        Response uncompressed = BrotliKt.uncompress(response);
        ResponseBody var10000 = uncompressed.body();
        String responseString = null;
        try {
            responseString = var10000 != null ? var10000.string() : null;
        } catch (IOException e) {
            e.fillInStackTrace();
        }
//        Assertions.assertThat(responseString).contains(new CharSequence[]{(CharSequence)"\"gzipped\": true,"});
//        Assertions.assertThat(responseString).contains(new CharSequence[]{(CharSequence)"\"Accept-Encoding\": \"br,gzip\""});
    }

    @Test
    public final void testNoUncompress() {
        Response response = response("https://httpbin.org/brotli", ByteString.encodeUtf8("XXXX"));
        Response same = BrotliKt.uncompress(response);
        ResponseBody var10000 = same.body();
        String responseString = null;
        try {
            responseString = var10000 != null ? var10000.string() : null;
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(responseString).isEqualTo("XXXX");
    }

    @Test
    public final void testFailsUncompress() {
        Response response = this.response("https://httpbin.org/brotli", ByteString.decodeHex("bb919aaa06e8"));

        try {
            Response failingResponse = BrotliKt.uncompress(response);
            ResponseBody var6 = failingResponse.body();
            if (var6 != null) {
                var6.string();
            }


        } catch (IOException var3) {
            String var5;
            label21: {
                Assertions.assertThat((Throwable)var3).hasMessage("Brotli stream decoding failed");
                Throwable var10000 = var3.getCause();
                if (var10000 != null) {
                    Class var4 = var10000.getClass();
                    if (var4 != null) {
                        var5 = var4.getSimpleName();
                        break label21;
                    }
                }

                var5 = null;
            }

            Assertions.assertThat(var5).isEqualTo("BrotliRuntimeException");
        }
    }

    @Test
    public final void testSkipUncompressNoContentResponse() {
        Response response = this.response("https://httpbin.org/brotli", ByteString.EMPTY);
        Response same = BrotliKt.uncompress(response);
        ResponseBody var10000 = same.body();
        String responseString = null;
        try {
            responseString = var10000 != null ? var10000.string() : null;
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(responseString).isEmpty();
    }

    private final Response response(String url, ByteString bodyHex) {
        Response.Builder var4 = (new Response.Builder()).body(ResponseBody.create( MediaType.get("text/plain"),bodyHex)).code(200).message("OK").request((new okhttp3.Request.Builder()).url(url).build()).protocol(Protocol.HTTP_2);
        return var4.build();
    }


}
