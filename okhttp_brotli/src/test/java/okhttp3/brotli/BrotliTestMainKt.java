package okhttp3.brotli;

import okhttp3.*;

import java.io.Closeable;
import java.io.IOException;

public class BrotliTestMainKt {
    public static final void main() {
        OkHttpClient client = (new OkHttpClient.Builder()).addInterceptor((Interceptor)BrotliInterceptor.INSTANCE).build();
        sendRequest("https://httpbin.org/brotli", client);
        sendRequest("https://httpbin.org/gzip", client);
    }

    // $FF: synthetic method
    public static void main(String[] var0) {
        main();
    }

    private static final void sendRequest(String url, OkHttpClient client) {
        Request req = (new okhttp3.Request.Builder()).url(url).build();
        Closeable var3 = null;
        try {
            var3 = (Closeable)client.newCall(req).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        boolean var4 = false;
        boolean var5 = false;
        Throwable var14 = (Throwable)null;

        try {
            Response it = (Response)var3;
            ResponseBody var10000 = it.body();
            String var8 = var10000 != null ? var10000.string() : null;
            boolean var9 = false;
            System.out.println(var8);
        } catch (Throwable var12) {
            var14 = var12;
            try {
                throw var12;
            } catch (IOException e) {
                e.fillInStackTrace();
            }
        } finally {
        }

    }
}
