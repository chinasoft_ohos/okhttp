package okhttp3.image;

import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import okhttp3.*;
import okhttp3.internal.publicsuffix.PublicSuffixData;
import okhttp3.internal.publicsuffix.PublicSuffixDatabase;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

import static okhttp3.internal.publicsuffix.PublicSuffixDatabase.PUBLIC_SUFFIX_RESOURCE;

public class SampleTest {
    public static final OkHttpClientTestRule clientRule = new OkHttpClientTestRule();

    public static void main(String[] var0) {
//        passingTest();
//        testMockWebServer(new MockWebServer());
//        testExternalSite(new MockWebServer());
        testPublicSuffixes();
        testExternalSite(new MockWebServer());
        testMockWebServer(new MockWebServer());
    }

    @Test
    public static final void passingTest() {
        System.out.println("ceshi----passingTest");
        Assertions.assertThat("hello").isEqualTo("hello");
    }

    @Test
    public static final void testMockWebServer(MockWebServer server) {
        server.enqueue((new MockResponse()).setBody("abc"));
        OkHttpClient client = clientRule.client;
        Closeable closeable = null;
        try {
            closeable = client.newCall((new Request.Builder()).url(server.url("/")).build()).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        if (null != closeable) {
            Response it = (Response) closeable;
            ResponseBody responseBody = it.body();
            try {
                Assertions.assertThat(responseBody.string()).isEqualTo("abc");
            } catch (IOException e) {
                e.fillInStackTrace();
            }
        }

    }

    @Test
    public static final void testExternalSite(MockWebServer server) {
//        server.enqueue((new MockResponse()).setBody("abc"));
        OkHttpClient client = clientRule.client;
        Closeable closeable = null;
        try {
            https:
            closeable = client.newCall((new Request.Builder()).url("https://www.baidu.com/img/bd_logo1.png").build()).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        if (null != closeable) {
            Response it = (Response) closeable;
            ResponseBody responseBody = it.body();
            System.out.println(it.toString());
//            Assertions.assertThat(it.code()).isEqualTo("200");
        }

    }

    @Test
    public static final void testPublicSuffixes() {
//        Closeable var1 = PublicSuffixDatabase.class.getResourceAsStream(PUBLIC_SUFFIX_RESOURCE);
        byte[] bytes = PublicSuffixData.to_byte(1);
        try {
            InputStream it = byte2InputStream(bytes);
            Assertions.assertThat(it.available()).isGreaterThan(30000);
        } catch (Throwable var8) {
        } finally {
        }

    }

    public static InputStream byte2InputStream(byte[] bytes) {
        return new ByteArrayInputStream(bytes);
    }

}
