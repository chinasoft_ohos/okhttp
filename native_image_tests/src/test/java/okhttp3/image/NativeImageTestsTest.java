package okhttp3.image;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.engine.descriptor.ClassBasedTestDescriptor;
import org.junit.platform.engine.TestDescriptor;
import org.junit.platform.engine.discovery.DiscoverySelectors;
import org.junit.platform.engine.discovery.PackageSelector;
import org.junit.platform.launcher.TestExecutionListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NativeImageTestsTest {
    @Test
    public final void testFindsFixedTestsForImage() {
        RunTests runTests=new RunTests();
        List testSelector = runTests.testSelectors(null);
        List x = RunTests.findTests(testSelector);
        for(int i=0;i<x.size();i++){
            if(x.get(i) instanceof ClassBasedTestDescriptor &&((ClassBasedTestDescriptor) x.get(i)).getTestClass()==SampleTest.class){
                return;
            }
        }

    }

    @Test
    public final void testFindsModuleTests() {
        PackageSelector testSelector = DiscoverySelectors.selectPackage("okhttp3");
        List x=new ArrayList();
        x.add(testSelector);
        for(int i=0;i<x.size();i++){
            if(x.get(i) instanceof ClassBasedTestDescriptor &&((ClassBasedTestDescriptor) x.get(i)).getTestClass()==SampleTest.class){
                return;
            }
        }
    }

    @Test
    public final void testFindsProjectTests() {
        PackageSelector testSelector = DiscoverySelectors.selectPackage("okhttp3");
        List x=new ArrayList();
        x.add(testSelector);
        for(int i=0;i<x.size();i++){
            if(x.get(i) instanceof ClassBasedTestDescriptor &&((ClassBasedTestDescriptor) x.get(i)).getTestClass()==SampleTest.class){
                return;
            }
        }
    }

    @Test
    public final void testTreeListener() {
        TestExecutionListener listener = RunTests.treeListener();
        Assertions.assertNotNull(listener);
    }
}
