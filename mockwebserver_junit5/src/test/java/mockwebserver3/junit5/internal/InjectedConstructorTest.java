package mockwebserver3.junit5.internal;

import mockwebserver3.MockWebServer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class InjectedConstructorTest {
    private  MockWebServer server=new MockWebServer();

    public static void main(String[] args){
        InjectedConstructorTest test=new InjectedConstructorTest();
        try {
            test.server.start();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        test.testOne();
        test.testTwo();
    }
    public final void testOne() {
        assertThat(server.started).isTrue();
    }

    public final void testTwo() {
        assertThat(this.server.started).isTrue();
    }

}
