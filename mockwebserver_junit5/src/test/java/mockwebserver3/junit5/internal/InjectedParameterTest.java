package mockwebserver3.junit5.internal;

import mockwebserver3.MockWebServer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class InjectedParameterTest {
    @Test
    public final void testOne( MockWebServer server) {
        Assertions.assertThat(server.started).isTrue();
    }

    @Test
    public final void testTwo( MockWebServer server) {
        Assertions.assertThat(server.started).isTrue();
    }
}
