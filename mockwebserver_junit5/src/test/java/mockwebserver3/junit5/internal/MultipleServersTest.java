package mockwebserver3.junit5.internal;

import mockwebserver3.MockWebServer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class MultipleServersTest {
    private final MockWebServer serverA=new MockWebServer();

    @Test
    public final void test( MockWebServer serverB) {
        Assertions.assertThat(this.serverA).isNotSameAs(serverB);
        Assertions.assertThat(this.serverA.started).isTrue();
        Assertions.assertThat(serverB.started).isTrue();
    }
}
