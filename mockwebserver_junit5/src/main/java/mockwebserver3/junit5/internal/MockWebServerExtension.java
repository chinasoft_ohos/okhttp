package mockwebserver3.junit5.internal;

import mockwebserver3.MockWebServer;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolver;

import java.io.IOException;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MockWebServerExtension implements BeforeAllCallback, BeforeEachCallback, AfterAllCallback, AfterEachCallback, ParameterResolver {
    private static final Logger logger = Logger.getLogger(MockWebServerExtension.class.getName());
    private static final ExtensionContext.Namespace namespace = ExtensionContext.Namespace.create(new Object[]{MockWebServerExtension.class});

    private final MockWebServerExtension.Resource getResource(ExtensionContext extensionContext) {
        ExtensionContext.Store store = extensionContext.getStore(namespace);
        MockWebServerExtension.Resource result = (MockWebServerExtension.Resource) store.get(extensionContext.getUniqueId());
        if (result == null) {
            result = new MockWebServerExtension.Resource();
            store.put(extensionContext.getUniqueId(), result);
        }

        return result;
    }

    public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
        Parameter var10000 = parameterContext.getParameter();
        return var10000.getType() == MockWebServer.class;
    }

    public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
        return this.getResource(extensionContext).newServer();
    }

    public void beforeAll(ExtensionContext context) {
        this.getResource(context).startAll();
    }

    public void beforeEach(ExtensionContext context) {
        this.getResource(context).startAll();
    }

    public void afterEach(ExtensionContext context) {
        this.getResource(context).shutdownAll();
    }

    public void afterAll(ExtensionContext context) {
        this.getResource(context).shutdownAll();
    }


    private static final class Resource {
        private final List servers;
        private boolean started;


        public final MockWebServer newServer() {
            MockWebServer mockWebServer = new MockWebServer();
            if (this.started) {
                try {
                    mockWebServer.start();
                } catch (IOException e) {
                    e.fillInStackTrace();
                }
            }

            Collection collection = (Collection) this.servers;
            collection.add(mockWebServer);
            return mockWebServer;
        }

        public final void startAll() {
            this.started = true;
            Iterator var2 = this.servers.iterator();

            while (var2.hasNext()) {
                MockWebServer server = (MockWebServer) var2.next();
                try {
                    server.start();
                } catch (IOException e) {
                    e.fillInStackTrace();
                }
            }

        }

        public final void shutdownAll() {
            try {
                Iterator var2 = this.servers.iterator();

                while (var2.hasNext()) {
                    MockWebServer server = (MockWebServer) var2.next();
                    server.shutdown();
                }
            } catch (IOException var3) {
                MockWebServerExtension.logger.log(Level.WARNING, "MockWebServer shutdown failed", (Throwable) var3);
            }

        }

        public Resource() {
            boolean var1 = false;
            this.servers = (List) (new ArrayList());
        }
    }


    public static final class Companion {
        private Companion() {
        }

    }
}
