package okhttp3.logging;

import okio.Buffer;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class IsProbablyUtf8Test {
    @Test
    public final void isProbablyUtf8() {
        Assertions.assertThat(Utf8.isProbablyUtf8(new Buffer())).isTrue();
        Assertions.assertThat(Utf8.isProbablyUtf8((new Buffer()).writeUtf8("abc"))).isTrue();
        Assertions.assertThat(Utf8.isProbablyUtf8((new Buffer()).writeUtf8("new\r\nlines"))).isTrue();
        Assertions.assertThat(Utf8.isProbablyUtf8((new Buffer()).writeUtf8("white\t space"))).isTrue();
        Assertions.assertThat(Utf8.isProbablyUtf8((new Buffer()).writeByte(128))).isTrue();
        Assertions.assertThat(Utf8.isProbablyUtf8((new Buffer()).writeByte(0))).isFalse();
        Assertions.assertThat(Utf8.isProbablyUtf8((new Buffer()).writeByte(192))).isFalse();
    }
}
