package okhttp3.logging;

import okio.Buffer;

import java.io.EOFException;

public class Utf8 {
    public static final boolean isProbablyUtf8( Buffer isProbablyUtf8) {
        try {
            Buffer prefix = new Buffer();
            long byteCount=0;
            if(isProbablyUtf8.size()>64L){
                byteCount=64L;
            }else{
                byteCount=isProbablyUtf8.size();

            }
            isProbablyUtf8.copyTo(prefix, 0L, byteCount);
            int var4 = 0;

            for(byte var5 = 16; var4 < var5 && !prefix.exhausted(); ++var4) {
                int codePoint = prefix.readUtf8CodePoint();
                if (Character.isISOControl(codePoint) && !Character.isWhitespace(codePoint)) {
                    return false;
                }
            }

            return true;
        } catch (EOFException var7) {
            return false;
        }
    }
}
