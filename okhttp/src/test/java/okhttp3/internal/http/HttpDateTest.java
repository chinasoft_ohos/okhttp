package okhttp3.internal.http;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.TimeZone;

public class HttpDateTest {
    private TimeZone originalDefault;

//    @BeforeEach
    public final void setUp() {
        TimeZone var10001 = TimeZone.getDefault();
        this.originalDefault = var10001;
        TimeZone.setDefault(TimeZone.getTimeZone("America/Los_Angeles"));
    }

//    @AfterEach
    public final void tearDown() throws Exception {
        TimeZone var10000 = this.originalDefault;
        if (var10000 == null) {
        }

        TimeZone.setDefault(var10000);
    }
    public static void main(String[] args) throws Exception {
        HttpDateTest test=new HttpDateTest();
        test.setUp();
        test.parseStandardFormats();
        test.format();
        test.parseNonStandardStrings();
    }
//    @Test
    public final void parseStandardFormats() throws Exception {
        Date var10000 = HttpDate.parse("Thu, 01 Jan 1970 00:00:00 GMT");
        System.out.println(var10000.getTime());
        Assertions.assertThat(var10000.getTime()).isEqualTo(0L);
        var10000 = HttpDate.parse("Fri, 06 Jun 2014 12:30:30 GMT");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(1402057830000L);
        var10000 = HttpDate.parse("Thursday, 01-Jan-70 00:00:00 GMT");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(0L);
        var10000 = HttpDate.parse("Friday, 06-Jun-14 12:30:30 GMT");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(1402057830000L);
        var10000 = HttpDate.parse("Thu Jan 1 00:00:00 1970");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(0L);
        var10000 = HttpDate.parse("Fri Jun 6 12:30:30 2014");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(1402057830000L);
    }

//    @Test
    public final void format() throws Exception {
        Assertions.assertThat(HttpDate.format(new Date(0L))).isEqualTo("Thu, 01 Jan 1970 00:00:00 GMT");
        System.out.println(HttpDate.format(new Date(0L)));

        Assertions.assertThat(HttpDate.format(new Date(1402057830000L))).isEqualTo("Fri, 06 Jun 2014 12:30:30 GMT");
        System.out.println(HttpDate.format(new Date(1402057830000L)));

    }

//    @Test
    public final void parseNonStandardStrings() throws Exception {
        Date var10000 = HttpDate.parse("Thu, 01 Jan 1970 00:00:00 GMT-01:00");
        Assertions.assertThat(var10000.getTime()).isEqualTo(3600000L);
        System.out.println(var10000.getTime());

        var10000 = HttpDate.parse("Thu, 01 Jan 1970 00:00:00 PST");
        Assertions.assertThat(var10000.getTime()).isEqualTo(28800000L);
        System.out.println(var10000.getTime());

        var10000 = HttpDate.parse("Thu, 01 Jan 1970 00:00:00 GMT JUNK");
        Assertions.assertThat(var10000.getTime()).isEqualTo(0L);
        System.out.println(var10000.getTime());

        Assertions.assertThat(HttpDate.parse("Thu, 01 Jan 1970 00:00:00")).isNull();
        Assertions.assertThat(HttpDate.parse("Thu, 01 Jan 1970 00:00 GMT")).isNull();
        Assertions.assertThat(HttpDate.parse("Thu,  01 Jan 1970 00:00 GMT")).isNull();
        Assertions.assertThat(HttpDate.parse("Thu, 1 Jan 1970 00:00 GMT")).isNull();
        var10000 = HttpDate.parse("Thursday, 01-Jan-1970 00:00:00 GMT-01:00");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(3600000L);
        var10000 = HttpDate.parse("Thursday, 01-Jan-1970 00:00:00 PST");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(28800000L);
        var10000 =HttpDate.parse("Thursday, 01-Jan-1970 00:00:00 PST JUNK");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(28800000L);
        var10000 = HttpDate.parse("Fri Jun 6 12:30:30 2014 PST");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(1402057830000L);
        var10000 = HttpDate.parse("Fri Jun 6 12:30:30 2014 JUNK");
        System.out.println(var10000.getTime());

        Assertions.assertThat(var10000.getTime()).isEqualTo(1402057830000L);
    }
}
