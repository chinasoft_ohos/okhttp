package okhttp3.internal;

import okio.BufferedSource;
import okio.Okio;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedHashMap;
import java.util.Map;

public class UtilTest {
    @Test
    public final void immutableMap() {
        LinkedHashMap map = new LinkedHashMap();
        ((Map)map).put("a", "A");
        Map immutableCopy = Util.immutableMap((Map)map);
        map.clear();

        try {
            if (immutableCopy == null) {
                throw new NullPointerException("null cannot be cast to non-null type ohos.collections.MutableMap<ohos.String, ohos.String>");
            }

        } catch (UnsupportedOperationException var4) {
        }

    }

    @Test
    public final void socketIsHealthy() throws IOException {
        InetAddress localhost = InetAddress.getLoopbackAddress();
        ServerSocket serverSocket = new ServerSocket(0, 1, localhost);
        Socket socket = new Socket();
        socket.connect(serverSocket.getLocalSocketAddress());
        BufferedSource socketSource = Okio.buffer(Okio.source(socket));
        serverSocket.close();
    }
}
