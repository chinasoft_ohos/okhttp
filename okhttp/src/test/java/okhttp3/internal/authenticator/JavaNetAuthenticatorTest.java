package okhttp3.internal.authenticator;

import junit.framework.TestCase;
import okhttp3.*;
import okhttp3.Authenticator;
import okhttp3.internal.RecordingAuthenticator;
import okhttp3.internal.proxy.NullProxySelector;
import okhttp3.internal.tls.OkHostnameVerifier;
import okhttp3.tls.internal.TlsUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

public class JavaNetAuthenticatorTest {
    private JavaNetAuthenticator authenticator = new JavaNetAuthenticator();
    private FakeDns fakeDns = new FakeDns();
    private RecordingAuthenticator recordingAuthenticator = new RecordingAuthenticator();

    public JavaNetAuthenticator getAuthenticator() {
        return this.authenticator;
    }

    public void setAuthenticator(JavaNetAuthenticator var1) {
        this.authenticator = var1;
    }

    public FakeDns getFakeDns() {
        return this.fakeDns;
    }

    public RecordingAuthenticator getRecordingAuthenticator() {
        return this.recordingAuthenticator;
    }

    @BeforeEach
    public void setup() {
//        Authenticator.setDefault((Authenticator)this.recordingAuthenticator);
    }

    @AfterEach
    public void tearDown() {
//        Authenticator.setDefault((Authenticator)null);
    }


    public static void main(String[] arg) {
        System.out.println("test----------------------------------------------");
        JavaNetAuthenticatorTest test=new JavaNetAuthenticatorTest();
        test.noSupportForNonBasicAuth();
    }

    @Test
    public void testBasicAuth() {
        List list = new ArrayList();
        try {
            list.add(InetAddress.getLocalHost());
            fakeDns.set("server", list);
        } catch (UnknownHostException e) {
            e.fillInStackTrace();
        }
        Dns var10004 = (Dns)fakeDns;
        List list1 = new ArrayList();
        List list2 = new ArrayList();
        list1.add(Protocol.HTTP_1_1);
        list2.add(ConnectionSpec.MODERN_TLS);
        SocketFactory var10005 = SocketFactory.getDefault();
        Address address = new Address("server", 443, var10004, var10005, TlsUtil.localhost().sslSocketFactory(), (HostnameVerifier) OkHostnameVerifier.INSTANCE, CertificatePinner.DEFAULT, okhttp3.Authenticator.NONE, Proxy.NO_PROXY, list1, list2, new NullProxySelector());
        Proxy var10003 = Proxy.NO_PROXY;
        InetSocketAddress var6 = InetSocketAddress.createUnresolved("server", 443);
        Route route = new Route(address, var10003, var6);
        Request request = (new Request.Builder()).url("https://www.mozilla.org/robots.txt").build();
        Response response = (new okhttp3.Response.Builder()).request(request).code(401).header("WWW-Authenticate", "Basic realm=\"User Visible Realm\"").protocol(Protocol.HTTP_2).message("Unauthorized").build();
        Request authRequest = null;
        try {
            authRequest = this.authenticator.authenticate(route, response);
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertEquals("Basic dXNlcm5hbWU6cGFzc3dvcmQ=", authRequest.header("Authorization"));
    }

    @Test
    public void noSupportForNonBasicAuth() {
        Request request = (new Request.Builder()).url("https://www.mozilla.org/robots.txt").build();
        Response response = (new okhttp3.Response.Builder()).request(request).code(401).header("WWW-Authenticate", "UnsupportedScheme realm=\"User Visible Realm\"").protocol(Protocol.HTTP_2).message("Unauthorized").build();
        Request authRequest = null;
        try {
            authRequest = this.authenticator.authenticate(null, response);
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        TestCase.assertNull(authRequest);
    }
}
