package okhttp3;

import mockwebserver3.MockWebServer;
import okhttp3.internal.platform.Platform;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.Closeable;
import java.io.IOException;

public class BouncyCastleTest {
    //todo notets
    public PlatformRule platform;
    public final OkHttpClientTestRule clientTestRule;
    private OkHttpClient client;
    private final MockWebServer server;

    public final OkHttpClient getClient() {
        return this.client;
    }

    public final void setClient( OkHttpClient var1) {
        this.client = var1;
    }

//    @BeforeEach
    public final void setUp() {
//        OkHttpDebugLogging.enable("org.bouncycastle.jsse");
//        this.platform.assumeBouncyCastle();
    }
public static void main(String[] args){
    BouncyCastleTest bouncyCastleTest=new BouncyCastleTest(new MockWebServer());
    bouncyCastleTest.testMozilla();
}
//    @Test
    public final void testMozilla() {
//        TestUtil.assumeNetwork();
        Request request = (new Request.Builder()).url("https://mozilla.org/robots.txt").build();
        Closeable var2 = null;
        try {
            var2 = (Closeable)this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }

        try {
            Response it = (Response)var2;
            Assertions.assertThat((Comparable)it.protocol()).isEqualTo(Protocol.HTTP_2);
            Handshake var10000 = it.handshake();
            AbstractComparableAssert var12 = (AbstractComparableAssert)Assertions.assertThat((Comparable)var10000.tlsVersion()).isEqualTo(TlsVersion.TLS_1_2);
        } catch (Throwable var9) {
            throw var9;
        } finally {
        }

    }

    public final MockWebServer getServer() {
        return this.server;
    }

    public BouncyCastleTest( MockWebServer server) {
        super();
        this.server = server;
        this.platform = new PlatformRule(null,null);
        this.clientTestRule = new OkHttpClientTestRule();
        this.client = this.clientTestRule.client;
    }
}
