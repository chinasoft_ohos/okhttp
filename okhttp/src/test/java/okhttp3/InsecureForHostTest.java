package okhttp3;

import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import okhttp3.internal.platform.Platform;
import okhttp3.tls.HandshakeCertificates;
import okhttp3.tls.HeldCertificate;
import okhttp3.tls.internal.TlsUtil;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AssertionsKt;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import javax.net.ssl.SSLException;
import java.io.IOException;
import java.security.cert.X509Certificate;

public class InsecureForHostTest {

    //todo 逻辑待测试
    @RegisterExtension
    public final PlatformRule platform;
    @RegisterExtension
    public final OkHttpClientTestRule clientTestRule;
    private final MockWebServer server;

    @Test
    public final void untrusted_host_in_insecureHosts_connects_successfully/* $FF was: untrusted host in insecureHosts connects successfully*/() {
        HandshakeCertificates serverCertificates = TlsUtil.localhost();
        this.server.useHttps(serverCertificates.sslSocketFactory(), false);
        this.server.enqueue(new MockResponse());
        HandshakeCertificates clientCertificates = null;
        OkHttpClient client = this.clientTestRule.client;
        client.newBuilder().sslSocketFactory(clientCertificates.sslSocketFactory(), clientCertificates.trustManager());        Call call = client.newCall((new okhttp3.Request.Builder()).url(this.server.url("/")).build());
        Response response = null;
        try {
            response = call.execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat(response.code()).isEqualTo(200);
        Handshake var10000 = response.handshake();
        Assertions.assertThat(var10000.cipherSuite()).isNotNull();
        var10000 = response.handshake();
        Assertions.assertThat((Comparable)var10000.tlsVersion()).isNotNull();
        var10000 = response.handshake();
        Assertions.assertThat(var10000.localCertificates()).isEmpty();
        var10000 = response.handshake();
        Assertions.assertThat(var10000.localPrincipal()).isNull();
        var10000 = response.handshake();
        Assertions.assertThat(var10000.peerCertificates()).isEmpty();
        var10000 = response.handshake();
        Assertions.assertThat(var10000.peerPrincipal()).isNull();
    }

    @Test
    public final void bad_certificates_host_in_insecureHosts_fails_with_SSLException/* $FF was: bad certificates host in insecureHosts fails with SSLException*/() {
        HeldCertificate heldCertificate = (new okhttp3.tls.HeldCertificate.Builder()).addSubjectAlternativeName("example.com").build();
        HandshakeCertificates serverCertificates = (new HandshakeCertificates.Builder()).heldCertificate(heldCertificate, new X509Certificate[0]).build();
        this.server.useHttps(serverCertificates.sslSocketFactory(), false);
        this.server.enqueue(new MockResponse());
        HandshakeCertificates clientCertificates = null;
        OkHttpClient client = this.clientTestRule.client;
        client.newBuilder().sslSocketFactory(clientCertificates.sslSocketFactory(), clientCertificates.trustManager());        Call call = client.newCall((new okhttp3.Request.Builder()).url(this.server.url("/")).build());

        try {
            try {
                call.execute();
            } catch (IOException e) {
                e.fillInStackTrace();
            }
        } catch (Exception var7) {
        }
    }

//    @Test
//    public final void untrusted_host_not_in_insecureHosts_fails_with_SSLException {
//        HandshakeCertificates serverCertificates = TlsUtil.localhost();
//        this.server.useHttps(serverCertificates.sslSocketFactory(), false);
//        this.server.enqueue(new MockResponse());
//        HandshakeCertificates clientCertificates = (new HandshakeCertificates.Builder()).addPlatformTrustedCertificates().addInsecureHost(this.server.getHostName() + '2').build();
//        OkHttpClient client = this.clientTestRule.client;
//        client.newBuilder().sslSocketFactory(clientCertificates.sslSocketFactory(), clientCertificates.trustManager());
//        Call call = client.newCall((new okhttp3.Request.Builder()).url(this.server.url("/")).build());
//
//        try {
//            try {
//                call.execute();
//            } catch (IOException e) {
//                e.fillInStackTrace();
//            }
//        } catch (Exception var6) {
//        }
//    }

    public final MockWebServer getServer() {
        return this.server;
    }

    public InsecureForHostTest(MockWebServer server) {
        super();
        this.server = server;
        this.platform = new PlatformRule("", null);
        this.clientTestRule = new OkHttpClientTestRule();
    }
}
