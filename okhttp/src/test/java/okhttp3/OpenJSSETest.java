package okhttp3;

import mockwebserver3.MockResponse;
import mockwebserver3.MockWebServer;
import okhttp3.internal.connection.Exchange;
import okhttp3.internal.connection.RealConnection;
import okhttp3.internal.platform.OpenJSSEPlatform;
import okhttp3.internal.platform.Platform;
import okhttp3.tls.HandshakeCertificates;
import okhttp3.tls.HeldCertificate;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.ObjectAssert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.openjsse.sun.security.ssl.SSLSocketFactoryImpl;
import org.openjsse.sun.security.ssl.SSLSocketImpl;

import java.io.Closeable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;

public class OpenJSSETest {
    //todo  跑不通过无法测试
    public PlatformRule platform;
    public final OkHttpClientTestRule clientTestRule;
    private OkHttpClient client;
    private final MockWebServer server;

    public final OkHttpClient getClient() {
        return this.client;
    }

    public final void setClient( OkHttpClient var1) {
        this.client = var1;
    }

    @BeforeEach
    public final void setUp() {
        this.platform.assumeOpenJSSE();
    }

    @Test
    public final void testTlsv13Works() {
        this.enableTls();
        this.server.enqueue((new MockResponse()).setBody("abc"));
        Request request = (new Request.Builder()).url(this.server.url("/")).build();
        Response response = null;
        try {
            response = this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Closeable var3 = (Closeable)response;
        boolean var4 = false;
        boolean var5 = false;
        Throwable var12 = (Throwable)null;

        try {
            Socket var15;
            label56: {
                Response it = (Response)var3;
                Assertions.assertEquals(200, response.code());
                TlsVersion var10000 = TlsVersion.TLS_1_3;
                Handshake var10001 = response.handshake();
                Assertions.assertEquals(var10000, var10001 != null ? var10001.tlsVersion() : null);
                Assertions.assertEquals(Protocol.HTTP_2, response.protocol());
                Exchange var13 = response.exchange;
                if (var13 != null) {
                    RealConnection var14 = var13.connection();
                    if (var14 != null) {
                        var15 = var14.socket();
                        break label56;
                    }
                }

                var15 = null;
            }

            ObjectAssert var16 = (ObjectAssert)org.assertj.core.api.Assertions.assertThat(var15).isInstanceOf(SSLSocketImpl.class);
        } catch (Throwable var10) {
            throw var10;
        } finally {
        }

    }

    @Test
    public final void testSupportedProtocols() {
        SSLSocketFactoryImpl factory = null;
        try {
            factory = new SSLSocketFactoryImpl();
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        Socket var10000 = factory.createSocket();
        if (var10000 == null) {
            throw new NullPointerException("null cannot be cast to non-null type org.openjsse.sun.security.ssl.SSLSocketImpl");
        } else {
            SSLSocketImpl s = (SSLSocketImpl)var10000;
            List var3 = Arrays.asList(new String[]{"TLSv1.3", "TLSv1.2", "TLSv1.1", "TLSv1"});
            String[] var10001 = s.getEnabledProtocols();
            Assertions.assertEquals(var3, Arrays.asList(var10001));
        }
    }

    @Test
    @Disabled
    public final void testMozilla() {
        Request request = (new Request.Builder()).url("https://mozilla.org/robots.txt").build();
        Closeable var2 = null;
        try {
            var2 = (Closeable)this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }

        Throwable var11 = (Throwable)null;

        try {
            Response it = (Response)var2;
            org.assertj.core.api.Assertions.assertThat((Comparable)it.protocol()).isEqualTo(Protocol.HTTP_2);
            Handshake var10000 = it.handshake();
            AbstractComparableAssert var12 = (AbstractComparableAssert)org.assertj.core.api.Assertions.assertThat((Comparable)var10000.tlsVersion()).isEqualTo(TlsVersion.TLS_1_3);
        } catch (Throwable var9) {
            var11 = var9;
            throw var9;
        } finally {
        }

    }

    @Test
    public final void testBuildIfSupported() {
        OpenJSSEPlatform actual = OpenJSSEPlatform.Companion.buildIfSupported();
        ObjectAssert var10000 = org.assertj.core.api.Assertions.assertThat(actual);
        var10000.isNotNull();
    }

    private final void enableTls() {
        okhttp3.tls.HeldCertificate.Builder var10000 = (new okhttp3.tls.HeldCertificate.Builder()).commonName("localhost");
        InetAddress var10001 = null;
        try {
            var10001 = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            e.fillInStackTrace();
        }
        String var3 = var10001.getCanonicalHostName();
        HeldCertificate heldCertificate = var10000.addSubjectAlternativeName(var3).build();
        HandshakeCertificates handshakeCertificates = (new okhttp3.tls.HandshakeCertificates.Builder()).heldCertificate(heldCertificate, new X509Certificate[0]).addTrustedCertificate(heldCertificate.certificate()).build();
        this.client = this.client.newBuilder().sslSocketFactory(handshakeCertificates.sslSocketFactory(), handshakeCertificates.trustManager()).build();
        this.server.useHttps(handshakeCertificates.sslSocketFactory(), false);
    }

    public final MockWebServer getServer() {
        return this.server;
    }

    public OpenJSSETest( MockWebServer server) {
        super();
        this.server = server;
        this.platform = new PlatformRule("", null);
        this.clientTestRule = new OkHttpClientTestRule();
        this.client = this.clientTestRule.client;
    }
}
