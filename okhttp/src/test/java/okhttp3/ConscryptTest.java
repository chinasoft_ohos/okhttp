package okhttp3;

import okhttp3.internal.platform.ConscryptPlatform;
import okhttp3.internal.platform.Platform;
import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ObjectAssert;
import org.conscrypt.Conscrypt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import javax.net.ssl.TrustManager;
import java.io.Closeable;
import java.io.IOException;

public class ConscryptTest {

    public static void main(String[] args) {
        ConscryptTest conscryptTest=new ConscryptTest();
        conscryptTest.setUp();
        conscryptTest.testBuildIfSupported();
        conscryptTest.testGaogle();
        conscryptTest.testMozilla();
        conscryptTest.testTrustManager();
        conscryptTest.testVersion();

    }
    public final PlatformRule platform;
    public final OkHttpClientTestRule clientTestRule;
    private final OkHttpClient client;

    public final void setUp() {
        this.platform.assumeConscrypt();
    }


    public final void testTrustManager() {
        Assertions.assertThat(Conscrypt.isConscrypt((TrustManager) Platform.get().platformTrustManager())).isTrue();
    }


    public final void testMozilla() {
        Request request = (new Request.Builder()).url("https://mozilla.org/robots.txt").build();
        Closeable var2 = null;
        try {
            var2 = (Closeable)this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        boolean var3 = false;
        boolean var4 = false;
        Throwable var11 = (Throwable)null;

        try {
            Response it = (Response)var2;
            Assertions.assertThat((Comparable)it.protocol()).isEqualTo(Protocol.HTTP_2);
            Handshake var10000 = it.handshake();
            AbstractComparableAssert var12 = (AbstractComparableAssert)Assertions.assertThat((Comparable)var10000.tlsVersion()).isEqualTo(TlsVersion.TLS_1_3);
        } catch (Throwable var9) {
            var11 = var9;
            throw var9;
        } finally {
        }

    }


    public final void testGaogle() {
        Request request = (new Request.Builder()).url("https://gaogle.com/robots.txt").build();
        Closeable var2 = null;
        try {
            var2 = (Closeable)this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        boolean var3 = false;
        boolean var4 = false;
        Throwable var11 = (Throwable)null;

        try {
            Response it = (Response)var2;
            Assertions.assertThat((Comparable)it.protocol()).isEqualTo(Protocol.HTTP_2);
            Handshake var10000 = it.handshake();
            if (var10000.tlsVersion() != TlsVersion.TLS_1_3) {
                System.err.println("Flaky TLSv1.3 with gaogle");
            }

        } catch (Throwable var9) {
            var11 = var9;
            throw var9;
        } finally {
        }

    }


    public final void testBuildIfSupported() {
        ConscryptPlatform actual = ConscryptPlatform.buildIfSupported();
        ObjectAssert var10000 = Assertions.assertThat(actual);
        var10000.isNotNull();
    }

    @Test
    public final void testVersion() {
        Conscrypt.Version version = Conscrypt.version();
//        org.junit.jupiter.api.Assertions.assertTrue(ConscryptPlatform.atLeastVersion(1, 4, 9));
//        org.junit.jupiter.api.Assertions.assertTrue(Companion.atLeastVersion$default(ConscryptPlatform.Companion, version.major(), 0, 0, 6, (Object)null));
//        org.junit.jupiter.api.Assertions.assertTrue(Companion.atLeastVersion$default(ConscryptPlatform.Companion, version.major(), version.minor(), 0, 4, (Object)null));
//        org.junit.jupiter.api.Assertions.assertTrue(ConscryptPlatform.Companion.atLeastVersion(version.major(), version.minor(), version.patch()));
//        org.junit.jupiter.api.Assertions.assertFalse(ConscryptPlatform.Companion.atLeastVersion(version.major(), version.minor(), version.patch() + 1));
//        org.junit.jupiter.api.Assertions.assertFalse(Companion.atLeastVersion$default(ConscryptPlatform.Companion, version.major(), version.minor() + 1, 0, 4, (Object)null));
//        org.junit.jupiter.api.Assertions.assertFalse(Companion.atLeastVersion$default(ConscryptPlatform.Companion, version.major() + 1, 0, 0, 6, (Object)null));
    }

    public ConscryptTest() {
        this.platform = PlatformRule.conscrypt();
        this.clientTestRule = new OkHttpClientTestRule();
        this.client = this.clientTestRule.client;
    }
}
