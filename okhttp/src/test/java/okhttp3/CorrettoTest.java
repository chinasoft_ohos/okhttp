package okhttp3;

import org.assertj.core.api.AbstractComparableAssert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.Closeable;
import java.io.IOException;

public class CorrettoTest {
    //todo  no test
    public final PlatformRule platform;

    public final OkHttpClientTestRule clientTestRule;
    private final OkHttpClient client;

    @BeforeEach
    public final void setUp() {
        this.platform.assumeConscrypt();
    }

    @Test
    @Disabled
    public final void testMozilla() {
        Request request = (new Request.Builder()).url("https://mozilla.org/robots.txt").build();
        Closeable var2 = null;
        try {
            var2 = (Closeable)this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        boolean var3 = false;
        boolean var4 = false;
        Throwable var11 = (Throwable)null;

        try {
            Response it = (Response)var2;
            Assertions.assertThat((Comparable)it.protocol()).isEqualTo(Protocol.HTTP_2);
            Handshake var10000 = it.handshake();
            AbstractComparableAssert var12 = (AbstractComparableAssert)Assertions.assertThat((Comparable)var10000.tlsVersion()).isEqualTo(TlsVersion.TLS_1_3);
        } catch (Throwable var9) {
            var11 = var9;
            throw var9;
        } finally {
        }

    }

    @Test
    @Disabled
    public final void testGaogle() {
        Request request = (new Request.Builder()).url("https://gaogle.com/robots.txt").build();
        Closeable var2 = null;
        try {
            var2 = (Closeable)this.client.newCall(request).execute();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        boolean var3 = false;
        boolean var4 = false;
        Throwable var11 = (Throwable)null;

        try {
            Response it = (Response)var2;
            Assertions.assertThat((Comparable)it.protocol()).isEqualTo(Protocol.HTTP_2);
            Handshake var10000 = it.handshake();
            if (var10000.tlsVersion() != TlsVersion.TLS_1_3) {
                System.err.println("Flaky TLSv1.3 with gaogle");
            }

        } catch (Throwable var9) {
            var11 = var9;
            throw var9;
        } finally {
        }

    }

    @Test
    public final void testIfSupported() {
//        Assertions.assertThat(PlatformRule.Companion.isCorrettoSupported()).isTrue();
//        Assertions.assertThat(PlatformRule.Companion.isCorrettoInstalled()).isTrue();
    }

    public CorrettoTest() {
        this.platform = PlatformRule.conscrypt();
        this.clientTestRule = new OkHttpClientTestRule();
        this.client = this.clientTestRule.client;
    }
}
