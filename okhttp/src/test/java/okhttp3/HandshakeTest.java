package okhttp3;

import okhttp3.tls.HeldCertificate;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HandshakeTest {
    //todo 无法测试
    private final HeldCertificate serverRoot = (new HeldCertificate.Builder()).certificateAuthority(1).build();
    private final HeldCertificate serverIntermediate;
    private final HeldCertificate serverCertificate;

    public final HeldCertificate getServerRoot() {
        return this.serverRoot;
    }

    public final HeldCertificate getServerIntermediate() {
        return this.serverIntermediate;
    }

    public final HeldCertificate getServerCertificate() {
        return this.serverCertificate;
    }

    @Test
    public final void createFromParts() {
//        Handshake  var10000 = Handshake.Companion;
        TlsVersion var10001 = TlsVersion.TLS_1_3;
        CipherSuite var10002 = CipherSuite.TLS_AES_128_GCM_SHA256;
        List var10003 = Arrays.asList(new X509Certificate[]{this.serverCertificate.certificate(), this.serverIntermediate.certificate()});
//        boolean var2 = false;
        List list=new ArrayList();
        Handshake handshake = Handshake.get(var10001, var10002, var10003,list );
        Assertions.assertThat((Comparable)handshake.tlsVersion()).isEqualTo(TlsVersion.TLS_1_3);
        Assertions.assertThat(handshake.cipherSuite()).isEqualTo(CipherSuite.TLS_AES_128_GCM_SHA256);
        Assertions.assertThat(handshake.peerCertificates()).containsExactly(new Certificate[]{(Certificate)this.serverCertificate.certificate(), (Certificate)this.serverIntermediate.certificate()});
        Assertions.assertThat(handshake.localPrincipal()).isNull();
        Assertions.assertThat(handshake.peerPrincipal()).isEqualTo(this.serverCertificate.certificate().getSubjectX500Principal());
        Assertions.assertThat(handshake.localCertificates()).isEmpty();
    }

    @Test
    public final void createFromSslSession() {
        HandshakeTest.FakeSSLSession sslSession = new HandshakeTest.FakeSSLSession("TLSv1.3", "TLS_AES_128_GCM_SHA256", new Certificate[]{(Certificate)this.serverCertificate.certificate(), (Certificate)this.serverIntermediate.certificate()}, (Certificate[])null);
        Handshake handshake = null;
        try {
            handshake = Handshake.get((SSLSession)sslSession);
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        Assertions.assertThat((Comparable)handshake.tlsVersion()).isEqualTo(TlsVersion.TLS_1_3);
        Assertions.assertThat(handshake.cipherSuite()).isEqualTo(CipherSuite.TLS_AES_128_GCM_SHA256);
        Assertions.assertThat(handshake.peerCertificates()).containsExactly(new Certificate[]{(Certificate)this.serverCertificate.certificate(), (Certificate)this.serverIntermediate.certificate()});
        Assertions.assertThat(handshake.localPrincipal()).isNull();
        Assertions.assertThat(handshake.peerPrincipal()).isEqualTo(this.serverCertificate.certificate().getSubjectX500Principal());
        Assertions.assertThat(handshake.localCertificates()).isEmpty();
    }

    @Test
    public final void sslWithNullNullNull() {
        HandshakeTest.FakeSSLSession sslSession = new HandshakeTest.FakeSSLSession("TLSv1.3", "SSL_NULL_WITH_NULL_NULL", new Certificate[]{(Certificate)this.serverCertificate.certificate(), (Certificate)this.serverIntermediate.certificate()}, (Certificate[])null);

        try {
            Handshake.get((SSLSession)sslSession);
            org.junit.jupiter.api.Assertions.fail();
        } catch (IOException var3) {
            Assertions.assertThat((Throwable)var3).hasMessage("cipherSuite == SSL_NULL_WITH_NULL_NULL");
        }

    }

    @Test
    public final void tlsWithNullNullNull() {
        HandshakeTest.FakeSSLSession sslSession = new HandshakeTest.FakeSSLSession("TLSv1.3", "TLS_NULL_WITH_NULL_NULL", new Certificate[]{(Certificate)this.serverCertificate.certificate(), (Certificate)this.serverIntermediate.certificate()}, (Certificate[])null);

        try {
            Handshake.get((SSLSession)sslSession);
            org.junit.jupiter.api.Assertions.fail();
        } catch (IOException var3) {
            Assertions.assertThat((Throwable)var3).hasMessage("cipherSuite == TLS_NULL_WITH_NULL_NULL");
        }

    }

    public HandshakeTest() {
        this.serverIntermediate = (new HeldCertificate.Builder()).certificateAuthority(0).signedBy(this.serverRoot).build();
        this.serverCertificate = (new HeldCertificate.Builder()).signedBy(this.serverIntermediate).build();
    }
    public static final class FakeSSLSession extends DelegatingSSLSession {
        private final String protocol;
        private final String cipherSuite;
        private final Certificate[] peerCertificates;
        private final Certificate[] localCertificates;

        public String getProtocol() {
            return this.protocol;
        }

        public String getCipherSuite() {
            return this.cipherSuite;
        }

        public Certificate[] getPeerCertificates() {
            return this.peerCertificates;
        }

        public Certificate[] getLocalCertificates() {
            return this.localCertificates;
        }

        public FakeSSLSession( String protocol,  String cipherSuite,  Certificate[] peerCertificates, Certificate[] localCertificates) {
            super(null);
            this.protocol = protocol;
            this.cipherSuite = cipherSuite;
            this.peerCertificates = peerCertificates;
            this.localCertificates = localCertificates;
        }
    }
}
