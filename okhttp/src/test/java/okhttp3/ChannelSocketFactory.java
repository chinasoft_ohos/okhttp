package okhttp3;

import javax.net.SocketFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.channels.SocketChannel;

public class ChannelSocketFactory  extends SocketFactory {
    public Socket createSocket() {
        Socket var10000 = null;
        try {
            var10000 = SocketChannel.open().socket();
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        return var10000;
    }

    public Socket createSocket( String host, int port) {
        String var3 = "Not yet implemented";
        try {
            throw new Exception("");
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return null;
    }

    public Socket createSocket( String host, int port,  InetAddress localHost, int localPort) {
        String var5 = "Not yet implemented";
        boolean var6 = false;
        return null;
    }

    public Socket createSocket( InetAddress host, int port) {
        String var3 = "Not yet implemented";
        boolean var4 = false;
        return null;
    }

    public Socket createSocket( InetAddress address, int port,  InetAddress localAddress, int localPort) {

        String var5 = "Not yet implemented";
        boolean var6 = false;
        return null;
    }
}
