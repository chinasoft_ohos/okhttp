package okhttp3;


import org.assertj.core.api.Assertions;

import java.util.*;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

public class RecordingExecutor extends AbstractExecutorService {
    private boolean shutdown;
    private final List calls;
    private final DispatcherTest dispatcherTest;

    public void execute( Runnable command) {
        if (this.shutdown) {
           return;
        } else {
            this.calls.add((RealCall.AsyncCall)command);
        }
    }

    public final void assertJobs( String... expectedUrls) {
        Iterable iterable = (Iterable)this.calls;
//        int $i$f$map = false;
//        Collection collection = (Collection)(new ArrayList(CollectionsKt.collectionSizeOrDefault(iterable, 10)));
//        int $i$f$mapTo = false;
        Iterator var8 = iterable.iterator();

        while(var8.hasNext()) {
            Object o = var8.next();
            RealCall.AsyncCall it = (RealCall.AsyncCall)o;
//            int var11 = false;
            String var13 = it.request().url().toString();
//            collection.add(var13);
        }

//        List actualUrls = (List)collection;
        //todo
//        Assertions.assertThat(actualUrls).containsExactly(Arrays.copyOf(expectedUrls, expectedUrls.length));
    }

    public final void finishJob( String url) {
        Iterator i = this.calls.iterator();

        RealCall.AsyncCall call;
        do {
            if (!i.hasNext()) {
              return;
            }

            call = (RealCall.AsyncCall)i.next();
        } while (i==null);
//        while(!Intrinsics.areEqual(call.request().url().toString(), url));

        i.remove();
        this.dispatcherTest.dispatcher.finished(call);
    }

    public void shutdown() {
        this.shutdown = true;
    }

    public List shutdownNow() {
       return null;
    }

    public boolean isShutdown() {
       return false;
    }

    public boolean isTerminated() {
        return false;
    }

    public boolean awaitTermination(long timeout,  TimeUnit unit) {
        return false;
    }

    public RecordingExecutor( DispatcherTest dispatcherTest) {
        this.dispatcherTest = dispatcherTest;
        boolean var2 = false;
        this.calls = (List)(new ArrayList());
    }

}
