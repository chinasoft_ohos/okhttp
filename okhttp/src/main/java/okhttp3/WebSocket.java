/*
 * Copyright (C) 2016 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package okhttp3;

import okio.ByteString;

import javax.annotation.Nullable;

/**
 * A non-blocking interface to a web socket. Use the {@linkplain Factory factory} to
 * create instances; usually this is {@link OkHttpClient}.
 *
 * <h3>Web Socket Lifecycle</h3>
 * <p>
 * Upon normal operation each web socket progresses through a sequence of states:
 *
 * <ul>
 *   <li><strong>Connecting:</strong> the initial state of each web socket. Messages may be enqueued
 *       but they won't be transmitted until the web socket is open.
 *   <li><strong>Open:</strong> the web socket has been accepted by the remote peer and is fully
 *       operational. Messages in either direction are enqueued for immediate transmission.
 *   <li><strong>Closing:</strong> one of the peers on the web socket has initiated a graceful
 *       shutdown. The web socket will continue to transmit already-enqueued messages but will
 *       refuse to enqueue new ones.
 *   <li><strong>Closed:</strong> the web socket has transmitted all of its messages and has
 *       received all messages from the peer.
 * </ul>
 * <p>
 * Web sockets may fail due to HTTP upgrade problems, connectivity problems, or if either peer
 * chooses to short-circuit the graceful shutdown process:
 *
 * <ul>
 *   <li><strong>Canceled:</strong> the web socket connection failed. Messages that were
 *       successfully enqueued by either peer may not have been transmitted to the other.
 * </ul>
 * <p>
 * Note that the state progression is independent for each peer. Arriving at a gracefully-closed
 * state indicates that a peer has sent all of its outgoing messages and received all of its
 * incoming messages. But it does not guarantee that the other peer will successfully receive all of
 * its incoming messages.
 */
public interface WebSocket {

    Request request();

    long queueSize();

    boolean send(String text);

    boolean send(ByteString bytes);

    boolean close(int code, @Nullable String reason);

    void cancel();

    interface Factory {
        WebSocket newWebSocket(Request request, WebSocketListener listener);
    }
}
