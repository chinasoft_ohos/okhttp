/** An HTTP+HTTP/2 client for Harmony and Java applications. */
@okhttp3.internal.annotations.EverythingIsNonNull
package okhttp3;
