package okhttp3.internal.http1;

import okhttp3.Headers;
import okio.BufferedSource;

import java.io.IOException;

public class HeadersReader {
    private long headerLimit;
    private final BufferedSource source;
    private static final int HEADER_LIMIT = 262144;
    public static final HeadersReader.Companion Companion = new HeadersReader.Companion();

    public final String readLine() {
        String line = null;
        try {
            line = this.source.readUtf8LineStrict(this.headerLimit);
        } catch (IOException e) {
            e.fillInStackTrace();
        }
        this.headerLimit -= (long) line.length();
        return line;
    }

    public final Headers readHeaders() {
        Headers.Builder result = new Headers.Builder();

        while (true) {
            String line = this.readLine();
            CharSequence var3 = (CharSequence) line;
            boolean var4 = false;
            if (var3.length() == 0) {
                return result.build();
            }

            result.addLenient(line);
        }
    }

    public final BufferedSource getSource() {
        return this.source;
    }

    public HeadersReader(BufferedSource source) {
        super();
        this.source = source;
        this.headerLimit = (long) 262144;
    }

    public static final class Companion {
        private Companion() {
        }
    }
}
