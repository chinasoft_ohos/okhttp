package okhttp3.internal.platform;

import okhttp3.Protocol;
import org.bouncycastle.jsse.BCSSLParameters;
import org.bouncycastle.jsse.BCSSLSocket;
import org.bouncycastle.jsse.provider.BouncyCastleJsseProvider;

import javax.annotation.Nullable;
import javax.net.ssl.*;
import java.io.IOException;
import java.security.*;
import java.util.List;

public class BouncyCastlePlatform extends Platform {
    private Provider provider;
    private static boolean isSupported;
    private static Companion companion = new Companion();

    private BouncyCastlePlatform() {
        this.provider = (Provider) (new BouncyCastleJsseProvider());
    }

    @Override
    public SSLContext getSSLContext() {
        try {
            return SSLContext.getInstance("TLS", this.provider);
        } catch (NoSuchAlgorithmException e) {
            e.fillInStackTrace();
        }
        return null;
    }

    @Override
    public X509TrustManager platformTrustManager() {
        try {
            TrustManagerFactory factory = TrustManagerFactory.getInstance("PKIX", "BCJSSE");
            try {
                factory.init((KeyStore) null);
                TrustManager[] trustManagers = factory.getTrustManagers();
                boolean istrue = trustManagers.length == 1 && trustManagers[0] instanceof X509TrustManager;
                if (!istrue) {
                    StringBuilder stringBuilder = (new StringBuilder()).append("Unexpected default trust managers: ");
                    try {
                        throw new Exception(stringBuilder.toString());
                    } catch (Exception e) {
                        e.fillInStackTrace();
                    }
                } else {
                    TrustManager var10 = trustManagers[0];
                    if (trustManagers[0] == null) {
                        throw new NullPointerException("null cannot be cast to non-null type javax.net.ssl.X509TrustManager");
                    } else {
                        return (X509TrustManager) var10;
                    }
                }
            } catch (KeyStoreException e) {
                e.fillInStackTrace();
            }
        } catch (NoSuchAlgorithmException e) {
            e.fillInStackTrace();
        } catch (NoSuchProviderException e) {
            e.fillInStackTrace();
        }

        return super.platformTrustManager();
    }

    @Nullable
    @Override
    protected X509TrustManager trustManager(SSLSocketFactory sslSocketFactory) {
        throw new UnsupportedOperationException("clientBuilder.sslSocketFactory(SSLSocketFactory) not supported with BouncyCastle");
    }

    @Override
    public void configureTlsExtensions(SSLSocket sslSocket, @Nullable String hostname, List<Protocol> protocols) throws IOException {
        if (sslSocket instanceof BCSSLSocket) {
            BCSSLParameters sslParameters = ((BCSSLSocket) sslSocket).getParameters();
            List names = alpnProtocolNames(protocols);
            Object[] object = new Object[names.size()];
            names.toArray(object);
            sslParameters.setApplicationProtocols((String[]) object);
            ((BCSSLSocket) sslSocket).setParameters(sslParameters);
        } else {
            super.configureTlsExtensions(sslSocket, hostname, protocols);
        }
    }

    @Nullable
    @Override
    public String getSelectedProtocol(SSLSocket sslSocket) {
        String string;
        if (sslSocket instanceof BCSSLSocket) {
            String protocol = ((BCSSLSocket) sslSocket).getApplicationProtocol();
            if (protocol != null) {
                switch (protocol.hashCode()) {
                    case 0:
                        if (protocol.equals("")) {
                            break;
                        }
                    default:
                        string = protocol;
                        return string;
                }
            }

            string = null;
        } else {
            string = super.getSelectedProtocol(sslSocket);
        }

        return string;
    }

    static {
        boolean isSupport;
        ClassLoader classLoader = companion.getClass().getClassLoader();
        try {
            Class.forName("org.bouncycastle.jsse.provider.BouncyCastleJsseProvider", false, classLoader);
            isSupport = true;
        } catch (ClassNotFoundException var2) {
            isSupport = false;
        }

        isSupported = isSupport;
    }

    public static class Companion {
        public static final boolean isSupported() {
            return BouncyCastlePlatform.isSupported;
        }

        public final BouncyCastlePlatform buildIfSupported() {
            return ((BouncyCastlePlatform.Companion) this).isSupported() ? new BouncyCastlePlatform() : null;
        }
    }
}
