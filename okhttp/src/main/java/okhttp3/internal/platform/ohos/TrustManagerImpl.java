package okhttp3.internal.platform.ohos;

import javax.net.ssl.*;
import java.net.Socket;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.*;
import java.util.*;
import java.util.logging.Logger;

public class TrustManagerImpl extends X509ExtendedTrustManager {
    private static final Logger logger = Logger.getLogger(TrustManagerImpl.class.getName());
    private static final TrustManagerImpl.TrustAnchorComparator TRUST_ANCHOR_COMPARATOR = new TrustManagerImpl.TrustAnchorComparator();
    //    private static ConscryptHostnameVerifier defaultHostnameVerifier;
    private final KeyStore rootKeyStore = null;
    //    private CertPinManager pinManager;
//    private final ConscryptCertStore trustedCertificateStore;
    private final CertPathValidator validator = null;
    //    private final TrustedCertificateIndex trustedCertificateIndex;
    private final TrustedCertificateIndex intermediateIndex = new TrustedCertificateIndex();
    ;
    private final X509Certificate[] acceptedIssuers = null;
    private final Exception err = null;
    private final CertificateFactory factory = null;
    //    private final CertBlacklist blacklist;
//    private CTVerifier ctVerifier;
//    private CTPolicy ctPolicy;
//    private ConscryptHostnameVerifier hostnameVerifier;
    private boolean ctEnabledOverride;

//    @UnsupportedAppUsage
//    public TrustManagerImpl(KeyStore keyStore) {
//        this(keyStore, (CertPinManager)null);
//    }
//
//    public TrustManagerImpl(KeyStore keyStore, CertPinManager manager) {
//        this(keyStore, manager, (ConscryptCertStore)null);
//    }
//
//    public TrustManagerImpl(KeyStore keyStore, CertPinManager manager, ConscryptCertStore certStore) {
//        this(keyStore, manager, certStore, (CertBlacklist)null);
//    }
//
//    public TrustManagerImpl(KeyStore keyStore, CertPinManager manager, ConscryptCertStore certStore, CertBlacklist blacklist) {
//        this(keyStore, manager, certStore, blacklist, (CTLogStore)null, (CTVerifier)null, (CTPolicy)null);
//    }
//
//    public TrustManagerImpl(KeyStore keyStore, CertPinManager manager, ConscryptCertStore certStore, CertBlacklist blacklist, CTLogStore ctLogStore, CTVerifier ctVerifier, CTPolicy ctPolicy) {
//        CertPathValidator validatorLocal = null;
//        CertificateFactory factoryLocal = null;
//        KeyStore rootKeyStoreLocal = null;
//        ConscryptCertStore trustedCertificateStoreLocal = null;
//        TrustedCertificateIndex trustedCertificateIndexLocal = null;
//        X509Certificate[] acceptedIssuersLocal = null;
//        Exception errLocal = null;
//
//        try {
//            validatorLocal = CertPathValidator.getInstance("PKIX");
//            factoryLocal = CertificateFactory.getInstance("X509");
//            if ("OhosCAStore".equals(keyStore.getType()) && Platform.supportsConscryptCertStore()) {
//                rootKeyStoreLocal = keyStore;
//                trustedCertificateStoreLocal = certStore != null ? certStore : Platform.newDefaultCertStore();
//                acceptedIssuersLocal = null;
//                trustedCertificateIndexLocal = new TrustedCertificateIndex();
//            } else {
//                rootKeyStoreLocal = null;
//                trustedCertificateStoreLocal = certStore;
//                acceptedIssuersLocal = acceptedIssuers(keyStore);
//                trustedCertificateIndexLocal = new TrustedCertificateIndex(trustAnchors(acceptedIssuersLocal));
//            }
//        } catch (Exception var16) {
//            errLocal = var16;
//        }
//
//        if (blacklist == null) {
//            blacklist = Platform.newDefaultBlacklist();
//        }
//
//        if (ctLogStore == null) {
//            ctLogStore = Platform.newDefaultLogStore();
//        }
//
//        if (ctPolicy == null) {
//            ctPolicy = Platform.newDefaultPolicy(ctLogStore);
//        }
//
//        this.pinManager = manager;
//        this.rootKeyStore = rootKeyStoreLocal;
//        this.trustedCertificateStore = trustedCertificateStoreLocal;
//        this.validator = validatorLocal;
//        this.factory = factoryLocal;
//        this.trustedCertificateIndex = trustedCertificateIndexLocal;
//        this.intermediateIndex = new TrustedCertificateIndex();
//        this.acceptedIssuers = acceptedIssuersLocal;
//        this.err = errLocal;
//        this.blacklist = blacklist;
//        this.ctVerifier = new CTVerifier(ctLogStore);
//        this.ctPolicy = ctPolicy;
//    }

    private static X509Certificate[] acceptedIssuers(KeyStore ks) {
        try {
            List<X509Certificate> trusted = new ArrayList();
            Enumeration en = ks.aliases();

            while (en.hasMoreElements()) {
                String alias = (String) en.nextElement();
                X509Certificate cert = (X509Certificate) ks.getCertificate(alias);
                if (cert != null) {
                    trusted.add(cert);
                }
            }

            return (X509Certificate[]) trusted.toArray(new X509Certificate[trusted.size()]);
        } catch (KeyStoreException var5) {
            return new X509Certificate[0];
        }
    }

    private static Set<TrustAnchor> trustAnchors(X509Certificate[] certs) {
        Set<TrustAnchor> trustAnchors = new HashSet(certs.length);
        X509Certificate[] var2 = certs;
        int var3 = certs.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            X509Certificate cert = var2[var4];
            trustAnchors.add(new TrustAnchor(cert, (byte[]) null));
        }

        return trustAnchors;
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        this.checkTrusted(chain, authType, (SSLSession) null, (SSLParameters) null, true);
    }

    public List<X509Certificate> checkClientTrusted(X509Certificate[] chain, String authType, String hostname) throws CertificateException {
        return this.checkTrusted(chain, (byte[]) null, (byte[]) null, authType, hostname, true);
    }

    private static SSLSession getHandshakeSessionOrThrow(SSLSocket sslSocket) throws CertificateException {
        SSLSession session = sslSocket.getHandshakeSession();
        if (session == null) {
            throw new CertificateException("Not in handshake; no session available");
        } else {
            return session;
        }
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {
        SSLSession session = null;
        SSLParameters parameters = null;
        if (socket instanceof SSLSocket) {
            SSLSocket sslSocket = (SSLSocket) socket;
            session = getHandshakeSessionOrThrow(sslSocket);
            parameters = sslSocket.getSSLParameters();
        }

        this.checkTrusted(chain, authType, session, parameters, true);
    }

    public void checkClientTrusted(X509Certificate[] chain, String authType, SSLEngine engine) throws CertificateException {
        SSLSession session = engine.getHandshakeSession();
        if (session == null) {
            throw new CertificateException("Not in handshake; no session available");
        } else {
            this.checkTrusted(chain, authType, session, engine.getSSLParameters(), true);
        }
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        this.checkTrusted(chain, authType, (SSLSession) null, (SSLParameters) null, false);
    }

    //    @UnsupportedAppUsage
    public List<X509Certificate> checkServerTrusted(X509Certificate[] chain, String authType, String hostname) throws CertificateException {
        return this.checkTrusted(chain, (byte[]) null, (byte[]) null, authType, hostname, false);
    }

    public List<X509Certificate> getTrustedChainForServer(X509Certificate[] certs, String authType, Socket socket) throws CertificateException {
        SSLSession session = null;
        SSLParameters parameters = null;
        if (socket instanceof SSLSocket) {
            SSLSocket sslSocket = (SSLSocket) socket;
            session = getHandshakeSessionOrThrow(sslSocket);
            parameters = sslSocket.getSSLParameters();
        }

        return this.checkTrusted(certs, authType, session, parameters, false);
    }

    public List<X509Certificate> getTrustedChainForServer(X509Certificate[] certs, String authType, SSLEngine engine) throws CertificateException {
        SSLSession session = engine.getHandshakeSession();
        if (session == null) {
            throw new CertificateException("Not in handshake; no session available");
        } else {
            return this.checkTrusted(certs, authType, session, engine.getSSLParameters(), false);
        }
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType, Socket socket) throws CertificateException {
        this.getTrustedChainForServer(chain, authType, socket);
    }

    public void checkServerTrusted(X509Certificate[] chain, String authType, SSLEngine engine) throws CertificateException {
        this.getTrustedChainForServer(chain, authType, engine);
    }

    public List<X509Certificate> checkServerTrusted(X509Certificate[] chain, String authType, SSLSession session) throws CertificateException {
        return this.checkTrusted(chain, authType, session, (SSLParameters) null, false);
    }

    public void handleTrustStorageUpdate() {
//        if (this.acceptedIssuers == null) {
//            this.trustedCertificateIndex.reset();
//        } else {
//            this.trustedCertificateIndex.reset(trustAnchors(this.acceptedIssuers));
//        }

    }

    private List<X509Certificate> checkTrusted(X509Certificate[] certs, String authType, SSLSession session, SSLParameters parameters, boolean clientAuth) throws CertificateException {
        byte[] ocspData = null;
        byte[] tlsSctData = null;
        String hostname = null;
        if (session != null) {
            hostname = session.getPeerHost();
            ocspData = this.getOcspDataFromSession(session);
            tlsSctData = this.getTlsSctDataFromSession(session);
        }

        if (session != null && parameters != null) {
            String identificationAlgorithm = parameters.getEndpointIdentificationAlgorithm();
            if ("HTTPS".equalsIgnoreCase(identificationAlgorithm)) {
//                ConscryptHostnameVerifier verifier = this.getHttpsVerifier();
//                if (!verifier.verify(hostname, session)) {
//                    throw new CertificateException("No subjectAltNames on the certificate match");
//                }
            }
        }

        return this.checkTrusted(certs, ocspData, tlsSctData, authType, hostname, clientAuth);
    }

    private byte[] getOcspDataFromSession(SSLSession session) {
        List<byte[]> ocspResponses = null;
//        if (session instanceof ConscryptSession) {
//            ConscryptSession opensslSession = (ConscryptSession)session;
//            ocspResponses = opensslSession.getStatusResponses();
//        } else {
//            try {
//                Method m_getResponses = session.getClass().getDeclaredMethod("getStatusResponses");
//                m_getResponses.setAccessible(true);
//                Object rawResponses = m_getResponses.invoke(session);
//                if (rawResponses instanceof List) {
//                    ocspResponses = (List)rawResponses;
//                }
//            } catch (NoSuchMethodException var5) {
//            } catch (SecurityException var6) {
//            } catch (IllegalAccessException var7) {
//            } catch (IllegalArgumentException var8) {
//            } catch (InvocationTargetException var9) {
//                throw new RuntimeException(var9.getCause());
//            }
//        }

        return ocspResponses != null && !ocspResponses.isEmpty() ? (byte[]) ocspResponses.get(0) : null;
    }

    private byte[] getTlsSctDataFromSession(SSLSession session) {
//        if (session instanceof ConscryptSession) {
//            ConscryptSession opensslSession = (ConscryptSession)session;
//            return opensslSession.getPeerSignedCertificateTimestamp();
//        } else {
//            byte[] data = null;
//
//            try {
//                Method m_getTlsSctData = session.getClass().getDeclaredMethod("getPeerSignedCertificateTimestamp");
//                m_getTlsSctData.setAccessible(true);
//                Object rawData = m_getTlsSctData.invoke(session);
//                if (rawData instanceof byte[]) {
//                    data = (byte[])rawData;
//                }
//            } catch (NoSuchMethodException var5) {
//            } catch (SecurityException var6) {
//            } catch (IllegalAccessException var7) {
//            } catch (IllegalArgumentException var8) {
//            } catch (InvocationTargetException var9) {
//                throw new RuntimeException(var9.getCause());
//            }

//            return data;
//        }
        return null;
    }

    private List<X509Certificate> checkTrusted(X509Certificate[] certs, byte[] ocspData, byte[] tlsSctData, String authType, String host, boolean clientAuth) throws CertificateException {
        if (certs != null && certs.length != 0 && authType != null && authType.length() != 0) {
            if (this.err != null) {
                throw new CertificateException(this.err);
            } else {
                Set<X509Certificate> used = new HashSet();
                ArrayList<X509Certificate> untrustedChain = new ArrayList();
                ArrayList<TrustAnchor> trustedChain = new ArrayList();
                X509Certificate leaf = certs[0];
                TrustAnchor leafAsAnchor = this.findTrustAnchorBySubjectAndPublicKey(leaf);
                if (leafAsAnchor != null) {
                    trustedChain.add(leafAsAnchor);
                    used.add(leafAsAnchor.getTrustedCert());
                } else {
                    untrustedChain.add(leaf);
                }

                used.add(leaf);
                return this.checkTrustedRecursive(certs, ocspData, tlsSctData, host, clientAuth, untrustedChain, trustedChain, used);
            }
        } else {
            throw new IllegalArgumentException("null or zero-length parameter");
        }
    }

    private List<X509Certificate> checkTrustedRecursive(X509Certificate[] certs, byte[] ocspData, byte[] tlsSctData, String host, boolean clientAuth, ArrayList<X509Certificate> untrustedChain, ArrayList<TrustAnchor> trustAnchorChain, Set<X509Certificate> used) throws CertificateException {
        CertificateException lastException = null;
        X509Certificate current;
        if (trustAnchorChain.isEmpty()) {
            current = (X509Certificate) untrustedChain.get(untrustedChain.size() - 1);
        } else {
            current = ((TrustAnchor) trustAnchorChain.get(trustAnchorChain.size() - 1)).getTrustedCert();
        }

        this.checkBlacklist(current);
        if (current.getIssuerDN().equals(current.getSubjectDN())) {
            return this.verifyChain(untrustedChain, trustAnchorChain, host, clientAuth, ocspData, tlsSctData);
        } else {
            Set<TrustAnchor> anchors = this.findAllTrustAnchorsByIssuerAndSignature(current);
            boolean seenIssuer = false;
            Iterator var13 = sortPotentialAnchors(anchors).iterator();

            while (true) {
                TrustAnchor anchor;
                X509Certificate anchorCert;
                do {
                    if (!var13.hasNext()) {
                        if (!trustAnchorChain.isEmpty()) {
                            if (!seenIssuer) {
                                return this.verifyChain(untrustedChain, trustAnchorChain, host, clientAuth, ocspData, tlsSctData);
                            }

                            throw lastException;
                        }

                        for (int i = 1; i < certs.length; ++i) {
                            X509Certificate candidateIssuer = certs[i];
                            if (!used.contains(candidateIssuer) && current.getIssuerDN().equals(candidateIssuer.getSubjectDN())) {
                                try {
                                    candidateIssuer.checkValidity();
//                                    ChainStrengthAnalyzer.checkCert(candidateIssuer);
                                } catch (CertificateException var20) {
                                    lastException = new CertificateException("Unacceptable certificate: " + candidateIssuer.getSubjectX500Principal(), var20);
                                    continue;
                                }

                                used.add(candidateIssuer);
                                untrustedChain.add(candidateIssuer);

                                try {
                                    return this.checkTrustedRecursive(certs, ocspData, tlsSctData, host, clientAuth, untrustedChain, trustAnchorChain, used);
                                } catch (CertificateException var19) {
                                    lastException = var19;
                                    used.remove(candidateIssuer);
                                    untrustedChain.remove(untrustedChain.size() - 1);
                                }
                            }
                        }

                        Set<TrustAnchor> intermediateAnchors = this.intermediateIndex.findAllByIssuerAndSignature(current);
                        Iterator var25 = sortPotentialAnchors(intermediateAnchors).iterator();

                        while (true) {
                            X509Certificate intermediateCert;
                            do {
                                if (!var25.hasNext()) {
                                    if (lastException != null) {
                                        throw lastException;
                                    }

                                    CertPath certPath = this.factory.generateCertPath(untrustedChain);
                                    throw new CertificateException(new CertPathValidatorException("Trust anchor for certification path not found.", (Throwable) null, certPath, -1));
                                }

                                TrustAnchor intermediate = (TrustAnchor) var25.next();
                                intermediateCert = intermediate.getTrustedCert();
                            } while (used.contains(intermediateCert));

                            used.add(intermediateCert);
                            untrustedChain.add(intermediateCert);

                            try {
                                return this.checkTrustedRecursive(certs, ocspData, tlsSctData, host, clientAuth, untrustedChain, trustAnchorChain, used);
                            } catch (CertificateException var18) {
                                lastException = var18;
                                untrustedChain.remove(untrustedChain.size() - 1);
                                used.remove(intermediateCert);
                            }
                        }
                    }

                    anchor = (TrustAnchor) var13.next();
                    anchorCert = anchor.getTrustedCert();
                } while (used.contains(anchorCert));

                seenIssuer = true;
                used.add(anchorCert);
                trustAnchorChain.add(anchor);

                try {
                    return this.checkTrustedRecursive(certs, ocspData, tlsSctData, host, clientAuth, untrustedChain, trustAnchorChain, used);
                } catch (CertificateException var21) {
                    lastException = var21;
                    trustAnchorChain.remove(trustAnchorChain.size() - 1);
                    used.remove(anchorCert);
                }
            }
        }
    }

    private List<X509Certificate> verifyChain(List<X509Certificate> untrustedChain, List<TrustAnchor> trustAnchorChain, String host, boolean clientAuth, byte[] ocspData, byte[] tlsSctData) throws CertificateException {
        try {
            CertPath certPath = this.factory.generateCertPath(untrustedChain);
            if (trustAnchorChain.isEmpty()) {
                throw new CertificateException(new CertPathValidatorException("Trust anchor for certification path not found.", (Throwable) null, certPath, -1));
            } else {
                List<X509Certificate> wholeChain = new ArrayList();
                wholeChain.addAll(untrustedChain);
                Iterator var9 = trustAnchorChain.iterator();

                while (var9.hasNext()) {
                    TrustAnchor anchor = (TrustAnchor) var9.next();
                    wholeChain.add(anchor.getTrustedCert());
                }

//                if (this.pinManager != null) {
//                    this.pinManager.checkChainPinning(host, wholeChain);
//                }

                var9 = wholeChain.iterator();

                while (var9.hasNext()) {
                    X509Certificate cert = (X509Certificate) var9.next();
                    this.checkBlacklist(cert);
                }

//                if (!clientAuth && (this.ctEnabledOverride || host != null && Platform.isCTVerificationRequired(host))) {
//                    this.checkCT(host, wholeChain, ocspData, tlsSctData);
//                }

                if (untrustedChain.isEmpty()) {
                    return wholeChain;
                } else {
//                    ChainStrengthAnalyzer.check(untrustedChain);

                    try {
                        Set<TrustAnchor> anchorSet = new HashSet();
                        anchorSet.add((TrustAnchor) trustAnchorChain.get(0));
                        PKIXParameters params = new PKIXParameters(anchorSet);
                        params.setRevocationEnabled(false);
                        X509Certificate endPointCert = (X509Certificate) untrustedChain.get(0);
                        this.setOcspResponses(params, endPointCert, ocspData);
                        params.addCertPathChecker(new TrustManagerImpl.ExtendedKeyUsagePKIXCertPathChecker(clientAuth, endPointCert));
                        this.validator.validate(certPath, params);
                    } catch (InvalidAlgorithmParameterException var12) {
                        throw new CertificateException("Chain validation failed", var12);
                    } catch (CertPathValidatorException var13) {
                        throw new CertificateException("Chain validation failed", var13);
                    }

                    for (int i = 1; i < untrustedChain.size(); ++i) {
                        this.intermediateIndex.index((X509Certificate) untrustedChain.get(i));
                    }

                    return wholeChain;
                }
            }
        } catch (CertificateException var14) {
            logger.fine("Rejected candidate cert chain due to error: " + var14.getMessage());
            throw var14;
        }
    }

    private void checkBlacklist(X509Certificate cert) throws CertificateException {
//        if (this.blacklist != null && this.blacklist.isPublicKeyBlackListed(cert.getPublicKey())) {
//            throw new CertificateException("Certificate blacklisted by public key: " + cert);
//        }
    }

    private void checkCT(String host, List<X509Certificate> chain, byte[] ocspData, byte[] tlsData) throws CertificateException {
//        CTVerificationResult result = this.ctVerifier.verifySignedCertificateTimestamps(chain, tlsData, ocspData);
//        if (!this.ctPolicy.doesResultConformToPolicy(result, host, (X509Certificate[])chain.toArray(new X509Certificate[chain.size()]))) {
//            throw new CertificateException("Certificate chain does not conform to required transparency policy.");
//        }
    }

    private void setOcspResponses(PKIXParameters params, X509Certificate cert, byte[] ocspData) {
        if (ocspData != null) {
            PKIXRevocationChecker revChecker = null;
            List<PKIXCertPathChecker> checkers = new ArrayList(params.getCertPathCheckers());
            Iterator var6 = checkers.iterator();

            while (var6.hasNext()) {
                PKIXCertPathChecker checker = (PKIXCertPathChecker) var6.next();
                if (checker instanceof PKIXRevocationChecker) {
                    revChecker = (PKIXRevocationChecker) checker;
                    break;
                }
            }

            if (revChecker == null) {
                try {
                    revChecker = (PKIXRevocationChecker) this.validator.getRevocationChecker();
                } catch (UnsupportedOperationException var8) {
                    return;
                }

                checkers.add(revChecker);
                revChecker.setOptions(Collections.singleton(PKIXRevocationChecker.Option.ONLY_END_ENTITY));
            }

            revChecker.setOcspResponses(Collections.singletonMap(cert, ocspData));
            params.setCertPathCheckers(checkers);
        }
    }

    private static Collection<TrustAnchor> sortPotentialAnchors(Set<TrustAnchor> anchors) {
        if (anchors.size() <= 1) {
            return anchors;
        } else {
            List<TrustAnchor> sortedAnchors = new ArrayList(anchors);
            Collections.sort(sortedAnchors, TRUST_ANCHOR_COMPARATOR);
            return sortedAnchors;
        }
    }

    private Set<TrustAnchor> findAllTrustAnchorsByIssuerAndSignature(X509Certificate cert) {
//        Set<TrustAnchor> indexedAnchors = this.trustedCertificateIndex.findAllByIssuerAndSignature(cert);
//        if (indexedAnchors.isEmpty() && this.trustedCertificateStore != null) {
//            Set<X509Certificate> storeAnchors = this.trustedCertificateStore.findAllIssuers(cert);
//            if (storeAnchors.isEmpty()) {
//                return indexedAnchors;
//            } else {
//                Set<TrustAnchor> result = new HashSet(storeAnchors.size());
//                Iterator var5 = storeAnchors.iterator();
//
//                while(var5.hasNext()) {
//                    X509Certificate storeCert = (X509Certificate)var5.next();
//                    result.add(this.trustedCertificateIndex.index(storeCert));
//                }
//
//                return result;
//            }
//        } else {
//            return indexedAnchors;
//        }
        return null;
    }

    private TrustAnchor findTrustAnchorBySubjectAndPublicKey(X509Certificate cert) {
//        TrustAnchor trustAnchor = this.trustedCertificateIndex.findBySubjectAndPublicKey(cert);
//        if (trustAnchor != null) {
//            return trustAnchor;
//        } else if (this.trustedCertificateStore == null) {
//            return null;
//        } else {
//            X509Certificate systemCert = this.trustedCertificateStore.getTrustAnchor(cert);
//            return systemCert != null ? new TrustAnchor(systemCert, (byte[])null) : null;
//        }
        return null;
    }

    public X509Certificate[] getAcceptedIssuers() {
        return this.acceptedIssuers != null ? (X509Certificate[]) this.acceptedIssuers.clone() : acceptedIssuers(this.rootKeyStore);
    }

//    static synchronized void setDefaultHostnameVerifier(ConscryptHostnameVerifier verifier) {
//        defaultHostnameVerifier = verifier;
//    }
//
//    static synchronized ConscryptHostnameVerifier getDefaultHostnameVerifier() {
//        return defaultHostnameVerifier;
//    }
//
//    void setHostnameVerifier(ConscryptHostnameVerifier verifier) {
//        this.hostnameVerifier = verifier;
//    }
//
//    ConscryptHostnameVerifier getHostnameVerifier() {
//        return this.hostnameVerifier;
//    }
//
//    private ConscryptHostnameVerifier getHttpsVerifier() {
//        if (this.hostnameVerifier != null) {
//            return this.hostnameVerifier;
//        } else {
//            ConscryptHostnameVerifier defaultVerifier = getDefaultHostnameVerifier();
//            return (ConscryptHostnameVerifier)(defaultVerifier != null ? defaultVerifier : TrustManagerImpl.GlobalHostnameVerifierAdapter.INSTANCE);
//        }
//    }

    public void setCTEnabledOverride(boolean enabled) {
        this.ctEnabledOverride = enabled;
    }

//    public void setCTVerifier(CTVerifier verifier) {
//        this.ctVerifier = verifier;
//    }
//
//    public void setCTPolicy(CTPolicy policy) {
//        this.ctPolicy = policy;
//    }
//
//    private static enum GlobalHostnameVerifierAdapter implements ConscryptHostnameVerifier {
//        INSTANCE;
//
//        private GlobalHostnameVerifierAdapter() {
//        }
//
//        public boolean verify(String hostname, SSLSession session) {
//            return HttpsURLConnection.getDefaultHostnameVerifier().verify(hostname, session);
//        }
//    }

    private static class ExtendedKeyUsagePKIXCertPathChecker extends PKIXCertPathChecker {
        private static final String EKU_OID = "2.5.29.37";
        private static final String EKU_anyExtendedKeyUsage = "2.5.29.37.0";
        private static final String EKU_clientAuth = "1.3.6.1.5.5.7.3.2";
        private static final String EKU_serverAuth = "1.3.6.1.5.5.7.3.1";
        private static final String EKU_nsSGC = "2.16.840.1.113730.4.1";
        private static final String EKU_msSGC = "1.3.6.1.4.1.311.10.3.3";
        private static final Set<String> SUPPORTED_EXTENSIONS = Collections.unmodifiableSet(new HashSet(Arrays.asList("2.5.29.37")));
        private final boolean clientAuth;
        private final X509Certificate leaf;

        private ExtendedKeyUsagePKIXCertPathChecker(boolean clientAuth, X509Certificate leaf) {
            this.clientAuth = clientAuth;
            this.leaf = leaf;
        }

        public void init(boolean forward) throws CertPathValidatorException {
        }

        public boolean isForwardCheckingSupported() {
            return true;
        }

        public Set<String> getSupportedExtensions() {
            return SUPPORTED_EXTENSIONS;
        }

        public void check(Certificate c, Collection<String> unresolvedCritExts) throws CertPathValidatorException {
            if (c == this.leaf) {
                List ekuOids;
                try {
                    ekuOids = this.leaf.getExtendedKeyUsage();
                } catch (CertificateParsingException var7) {
                    throw new CertPathValidatorException(var7);
                }

                if (ekuOids != null) {
                    boolean goodExtendedKeyUsage = false;
                    Iterator var5 = ekuOids.iterator();

                    while (var5.hasNext()) {
                        String ekuOid = (String) var5.next();
                        if (ekuOid.equals("2.5.29.37.0")) {
                            goodExtendedKeyUsage = true;
                            break;
                        }

                        if (this.clientAuth) {
                            if (ekuOid.equals("1.3.6.1.5.5.7.3.2")) {
                                goodExtendedKeyUsage = true;
                                break;
                            }
                        } else {
                            if (ekuOid.equals("1.3.6.1.5.5.7.3.1")) {
                                goodExtendedKeyUsage = true;
                                break;
                            }

                            if (ekuOid.equals("2.16.840.1.113730.4.1")) {
                                goodExtendedKeyUsage = true;
                                break;
                            }

                            if (ekuOid.equals("1.3.6.1.4.1.311.10.3.3")) {
                                goodExtendedKeyUsage = true;
                                break;
                            }
                        }
                    }

                    if (goodExtendedKeyUsage) {
                        unresolvedCritExts.remove("2.5.29.37");
                    } else {
                        throw new CertPathValidatorException("End-entity certificate does not have a valid extendedKeyUsage.");
                    }
                }
            }
        }
    }

    private static class TrustAnchorComparator implements Comparator<TrustAnchor> {
        private static final CertificatePriorityComparator CERT_COMPARATOR = new CertificatePriorityComparator();

        private TrustAnchorComparator() {
        }

        public int compare(TrustAnchor lhs, TrustAnchor rhs) {
            X509Certificate lhsCert = lhs.getTrustedCert();
            X509Certificate rhsCert = rhs.getTrustedCert();
            return CERT_COMPARATOR.compare(lhsCert, rhsCert);
        }
    }
}

