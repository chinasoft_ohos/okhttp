package okhttp3.internal.platform.ohos;


import javax.annotation.Nullable;
import java.lang.reflect.Method;

public class CloseGuard {
    private Method getMethod = null;
    private Method openMethod = null;
    private Method warnIfOpenMethod = null;

    public CloseGuard(@Nullable Method getMethod, @Nullable Method openMethod, @Nullable Method warnIfOpenMethod) {
        this.getMethod = getMethod;
        this.openMethod = openMethod;
        this.warnIfOpenMethod = warnIfOpenMethod;
    }

    public final Object createAndOpen(String closer) {
        if (this.getMethod != null) {
            try {
                Object closeGuardInstance = this.getMethod.invoke((Object) null);
                Method method = this.openMethod;
                method.invoke(closeGuardInstance, closer);
                return closeGuardInstance;
            } catch (Exception var3) {
            }
        }

        return null;
    }

    public final boolean warnIfOpen(@Nullable Object closeGuardInstance) {
        boolean reported = false;
        if (closeGuardInstance != null) {
            try {
                Method method = this.warnIfOpenMethod;
                method.invoke(closeGuardInstance);
                reported = true;
            } catch (Exception var4) {
            }
        }

        return reported;
    }

    class Companion {
        public final CloseGuard get() {
            Method getMethod = null;
            Method openMethod = null;
            Method warnIfOpenMethod = null;

            try {
                Class closeGuardClass = Class.forName("dalvik.system.CloseGuard");
                getMethod = closeGuardClass.getMethod("get");
                openMethod = closeGuardClass.getMethod("open", String.class);
                warnIfOpenMethod = closeGuardClass.getMethod("warnIfOpen");
            } catch (Exception var5) {
                getMethod = (Method) null;
                openMethod = (Method) null;
                warnIfOpenMethod = (Method) null;
            }

            return new CloseGuard(getMethod, openMethod, warnIfOpenMethod);
        }

        private Companion() {
        }
    }
}
