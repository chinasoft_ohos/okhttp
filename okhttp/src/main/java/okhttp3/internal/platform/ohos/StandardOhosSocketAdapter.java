package okhttp3.internal.platform.ohos;


import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;

public class StandardOhosSocketAdapter extends OhosSocketAdapter {
    private final Class sslSocketFactoryClass;
    private final Class paramClass;

    public StandardOhosSocketAdapter(Class sslSocketClass, Class sslSocketFactoryClass, Class paramClass) {
        super(sslSocketClass);
        this.sslSocketFactoryClass = sslSocketFactoryClass;
        this.paramClass = paramClass;
    }

    @Override
    public Boolean matchesSocketFactory(SSLSocketFactory sslSocketFactory) {
        return this.sslSocketFactoryClass.isInstance(sslSocketFactory);
    }

    @Override
    public X509TrustManager trustManager(SSLSocketFactory sslSocketFactory) {
        Object context = readFieldOrNull(sslSocketFactory, this.paramClass, "sslParameters");
        X509TrustManager x509TrustManager = (X509TrustManager) readFieldOrNull(context, X509TrustManager.class, "x509TrustManager");
        X509TrustManager x509TrustManager1 = x509TrustManager;
        if (x509TrustManager == null) {
            x509TrustManager1 = (X509TrustManager) readFieldOrNull(context, X509TrustManager.class, "trustManager");
        }

        return x509TrustManager1;
    }

    class Companion {
        public final SocketAdapter buildIfSupported(String packageName) {
            SocketAdapter socketAdapter;
            try {
                Class clazz = Class.forName(packageName + ".OpenSSLSocketImpl");
                if (clazz == null) {
                    throw new NullPointerException("null cannot be cast to non-null type java.lang.Class<in javax.net.ssl.SSLSocket>");
                }

                Class sslSocketClass = clazz;
                clazz = Class.forName(packageName + ".OpenSSLSocketFactoryImpl");
                if (clazz == null) {
                    throw new NullPointerException("null cannot be cast to non-null type java.lang.Class<in javax.net.ssl.SSLSocketFactory>");
                }

                Class sslSocketFactoryClass = clazz;
                Class paramsClass = Class.forName(packageName + ".SSLParametersImpl");
                socketAdapter = (SocketAdapter) (new StandardOhosSocketAdapter(sslSocketClass, sslSocketFactoryClass, paramsClass));
            } catch (Exception var7) {
                socketAdapter = null;
            }

            return socketAdapter;
        }

        private Companion() {
        }
    }
}
