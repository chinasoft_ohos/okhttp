package okhttp3.internal.platform.ohos;

import okhttp3.Protocol;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import java.util.List;

public interface SocketAdapter {
    public Boolean isSupported();

    public X509TrustManager trustManager(SSLSocketFactory sslSocketFactory);

    public Boolean matchesSocket(SSLSocket sslSocket);

    public Boolean matchesSocketFactory(SSLSocketFactory sslSocketFactory);

    public void configureTlsExtensions(
            SSLSocket sslSocket,
            String hostname,
            List<Protocol> protocols);

    public String getSelectedProtocol(SSLSocket sslSocket);
}
