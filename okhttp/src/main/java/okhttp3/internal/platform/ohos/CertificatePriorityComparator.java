package okhttp3.internal.platform.ohos;

import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.interfaces.ECPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public final class CertificatePriorityComparator implements Comparator<X509Certificate> {
    private static final Map<String, Integer> ALGORITHM_OID_PRIORITY_MAP = new HashMap();
    private static final Integer PRIORITY_MD5 = 6;
    private static final Integer PRIORITY_SHA1 = 5;
    private static final Integer PRIORITY_SHA224 = 4;
    private static final Integer PRIORITY_SHA256 = 3;
    private static final Integer PRIORITY_SHA384 = 2;
    private static final Integer PRIORITY_SHA512 = 1;
    private static final Integer PRIORITY_UNKNOWN = -1;

    public CertificatePriorityComparator() {
    }

    public int compare(X509Certificate lhs, X509Certificate rhs) {
        boolean lhsSelfSigned = lhs.getSubjectDN().equals(lhs.getIssuerDN());
        boolean rhsSelfSigned = rhs.getSubjectDN().equals(rhs.getIssuerDN());
        if (lhsSelfSigned != rhsSelfSigned) {
            return rhsSelfSigned ? 1 : -1;
        } else {
            int result = this.compareStrength(rhs, lhs);
            if (result != 0) {
                return result;
            } else {
                Date lhsNotAfter = lhs.getNotAfter();
                Date rhsNotAfter = rhs.getNotAfter();
                result = rhsNotAfter.compareTo(lhsNotAfter);
                if (result != 0) {
                    return result;
                } else {
                    Date lhsNotBefore = lhs.getNotBefore();
                    Date rhsNotBefore = rhs.getNotBefore();
                    return rhsNotBefore.compareTo(lhsNotBefore);
                }
            }
        }
    }

    private int compareStrength(X509Certificate lhs, X509Certificate rhs) {
        PublicKey lhsPublicKey = lhs.getPublicKey();
        PublicKey rhsPublicKey = rhs.getPublicKey();
        int result = this.compareKeyAlgorithm(lhsPublicKey, rhsPublicKey);
        if (result != 0) {
            return result;
        } else {
            result = this.compareKeySize(lhsPublicKey, rhsPublicKey);
            return result != 0 ? result : this.compareSignatureAlgorithm(lhs, rhs);
        }
    }

    private int compareKeyAlgorithm(PublicKey lhs, PublicKey rhs) {
        String lhsAlgorithm = lhs.getAlgorithm();
        String rhsAlgorithm = rhs.getAlgorithm();
        if (lhsAlgorithm.equalsIgnoreCase(rhsAlgorithm)) {
            return 0;
        } else {
            return "EC".equalsIgnoreCase(lhsAlgorithm) ? 1 : -1;
        }
    }

    private int compareKeySize(PublicKey lhs, PublicKey rhs) {
        String lhsAlgorithm = lhs.getAlgorithm();
        String rhsAlgorithm = rhs.getAlgorithm();
        if (!lhsAlgorithm.equalsIgnoreCase(rhsAlgorithm)) {
            throw new IllegalArgumentException("Keys are not of the same type");
        } else {
            int lhsSize = this.getKeySize(lhs);
            int rhsSize = this.getKeySize(rhs);
            return lhsSize - rhsSize;
        }
    }

    private int getKeySize(PublicKey pkey) {
        if (pkey instanceof ECPublicKey) {
            return ((ECPublicKey) pkey).getParams().getCurve().getField().getFieldSize();
        } else if (pkey instanceof RSAPublicKey) {
            return ((RSAPublicKey) pkey).getModulus().bitLength();
        } else {
            throw new IllegalArgumentException("Unsupported public key type: " + pkey.getClass().getName());
        }
    }

    private int compareSignatureAlgorithm(X509Certificate lhs, X509Certificate rhs) {
        Integer lhsPriority = (Integer) ALGORITHM_OID_PRIORITY_MAP.get(lhs.getSigAlgOID());
        Integer rhsPriority = (Integer) ALGORITHM_OID_PRIORITY_MAP.get(rhs.getSigAlgOID());
        if (lhsPriority == null) {
            lhsPriority = PRIORITY_UNKNOWN;
        }

        if (rhsPriority == null) {
            rhsPriority = PRIORITY_UNKNOWN;
        }

        return rhsPriority - lhsPriority;
    }

    static {
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.113549.1.1.13", PRIORITY_SHA512);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.113549.1.1.12", PRIORITY_SHA384);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.113549.1.1.11", PRIORITY_SHA256);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.113549.1.1.14", PRIORITY_SHA224);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.113549.1.1.5", PRIORITY_SHA1);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.113549.1.1.4", PRIORITY_MD5);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.10045.4.3.4", PRIORITY_SHA512);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.10045.4.3.3", PRIORITY_SHA384);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.10045.4.3.2", PRIORITY_SHA256);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.10045.4.3.1", PRIORITY_SHA224);
        ALGORITHM_OID_PRIORITY_MAP.put("1.2.840.10045.4.1", PRIORITY_SHA1);
    }
}

