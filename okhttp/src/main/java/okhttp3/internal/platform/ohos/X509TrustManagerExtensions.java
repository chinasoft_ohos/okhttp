package okhttp3.internal.platform.ohos;

import java.lang.reflect.InvocationTargetException;
import javax.net.ssl.X509TrustManager;
import java.lang.reflect.Method;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

public class X509TrustManagerExtensions {
    private final TrustManagerImpl mDelegate;
    private final X509TrustManager mTrustManager;
    private final Method mCheckServerTrusted;
    private final Method mIsSameTrustConfiguration;

    public X509TrustManagerExtensions(X509TrustManager tm) throws IllegalArgumentException {
        if (tm instanceof TrustManagerImpl) {
            this.mDelegate = (TrustManagerImpl) tm;
            this.mTrustManager = null;
            this.mCheckServerTrusted = null;
            this.mIsSameTrustConfiguration = null;
        } else {
            this.mDelegate = null;
            this.mTrustManager = tm;

            try {
                this.mCheckServerTrusted = tm.getClass().getMethod("checkServerTrusted", X509Certificate[].class, String.class, String.class);
            } catch (NoSuchMethodException var5) {
                throw new IllegalArgumentException("Required method checkServerTrusted(X509Certificate[], String, String, String) missing");
            }

            Method isSameTrustConfiguration = null;

            try {
                isSameTrustConfiguration = tm.getClass().getMethod("isSameTrustConfiguration", String.class, String.class);
            } catch (NoSuchMethodException e) {
                e.fillInStackTrace();
            }


            this.mIsSameTrustConfiguration = isSameTrustConfiguration;
        }
    }

    public List<X509Certificate> checkServerTrusted(X509Certificate[] chain, String authType, String host) throws CertificateException {
        if (this.mDelegate != null) {
            return this.mDelegate.checkServerTrusted(chain, authType, host);
        } else {
            try {
                return (List) this.mCheckServerTrusted.invoke(this.mTrustManager, chain, authType, host);
            } catch (IllegalAccessException var5) {
                throw new CertificateException("Failed to call checkServerTrusted", var5);
            } catch (InvocationTargetException var6) {
                if (var6.getCause() instanceof CertificateException) {
                    throw (CertificateException) var6.getCause();
                } else if (var6.getCause() instanceof RuntimeException) {
                    throw (RuntimeException) var6.getCause();
                } else {
                    throw new CertificateException("checkServerTrusted failed", var6.getCause());
                }
            }
        }
    }

    public boolean isSameTrustConfiguration(String hostname1, String hostname2) {
        if (this.mIsSameTrustConfiguration == null) {
            return true;
        } else {
            try {
                return (Boolean) this.mIsSameTrustConfiguration.invoke(this.mTrustManager, hostname1, hostname2);
            } catch (IllegalAccessException var4) {
                throw new RuntimeException("Failed to call isSameTrustConfiguration", var4);
            } catch (InvocationTargetException var5) {
                if (var5.getCause() instanceof RuntimeException) {
                    throw (RuntimeException) var5.getCause();
                } else {
                    throw new RuntimeException("isSameTrustConfiguration failed", var5.getCause());
                }
            }
        }
    }
}
