package okhttp3.internal.platform.ohos;

import javax.security.auth.x500.X500Principal;
import java.security.PublicKey;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.util.*;

public final class TrustedCertificateIndex {
    private final Map<X500Principal, List<TrustAnchor>> subjectToTrustAnchors = new HashMap();

    public TrustedCertificateIndex() {
    }

    public TrustedCertificateIndex(Set<TrustAnchor> anchors) {
        this.index(anchors);
    }

    private void index(Set<TrustAnchor> anchors) {
        Iterator var2 = anchors.iterator();

        while (var2.hasNext()) {
            TrustAnchor anchor = (TrustAnchor) var2.next();
            this.index(anchor);
        }

    }

    public TrustAnchor index(X509Certificate cert) {
        TrustAnchor anchor = new TrustAnchor(cert, (byte[]) null);
        this.index(anchor);
        return anchor;
    }

    public void index(TrustAnchor anchor) {
        X509Certificate cert = anchor.getTrustedCert();
        X500Principal subject;
        if (cert != null) {
            subject = cert.getSubjectX500Principal();
        } else {
            subject = anchor.getCA();
        }

        synchronized (this.subjectToTrustAnchors) {
            List<TrustAnchor> anchors = (List) this.subjectToTrustAnchors.get(subject);
            if (anchors == null) {
                anchors = new ArrayList(1);
                this.subjectToTrustAnchors.put(subject, anchors);
            } else if (cert != null) {
                Iterator var6 = ((List) anchors).iterator();

                while (var6.hasNext()) {
                    TrustAnchor entry = (TrustAnchor) var6.next();
                    if (cert.equals(entry.getTrustedCert())) {
                        return;
                    }
                }
            }

            ((List) anchors).add(anchor);
        }
    }

    public void reset() {
        synchronized (this.subjectToTrustAnchors) {
            this.subjectToTrustAnchors.clear();
        }
    }

    public void reset(Set<TrustAnchor> anchors) {
        synchronized (this.subjectToTrustAnchors) {
            this.reset();
            this.index(anchors);
        }
    }

    public TrustAnchor findByIssuerAndSignature(X509Certificate cert) {
        X500Principal issuer = cert.getIssuerX500Principal();
        synchronized (this.subjectToTrustAnchors) {
            List<TrustAnchor> anchors = (List) this.subjectToTrustAnchors.get(issuer);
            if (anchors == null) {
                return null;
            } else {
                Iterator var5 = anchors.iterator();

                while (true) {
                    if (var5.hasNext()) {
                        TrustAnchor anchor = (TrustAnchor) var5.next();

                        TrustAnchor var10000;
                        try {
                            X509Certificate caCert = anchor.getTrustedCert();
                            PublicKey publicKey;
                            if (caCert != null) {
                                publicKey = caCert.getPublicKey();
                            } else {
                                publicKey = anchor.getCAPublicKey();
                            }

                            cert.verify(publicKey);
                            var10000 = anchor;
                        } catch (Exception var10) {
                            continue;
                        }

                        return var10000;
                    }

                    return null;
                }
            }
        }
    }

    public TrustAnchor findBySubjectAndPublicKey(X509Certificate cert) {
        X500Principal subject = cert.getSubjectX500Principal();
        synchronized (this.subjectToTrustAnchors) {
            List<TrustAnchor> anchors = (List) this.subjectToTrustAnchors.get(subject);
            return anchors == null ? null : findBySubjectAndPublicKey(cert, anchors);
        }
    }

    private static TrustAnchor findBySubjectAndPublicKey(X509Certificate cert, Collection<TrustAnchor> anchors) {
        PublicKey certPublicKey = cert.getPublicKey();
        Iterator var3 = anchors.iterator();

        while (var3.hasNext()) {
            TrustAnchor anchor = (TrustAnchor) var3.next();

            try {
                X509Certificate caCert = anchor.getTrustedCert();
                PublicKey caPublicKey;
                if (caCert != null) {
                    caPublicKey = caCert.getPublicKey();
                } else {
                    caPublicKey = anchor.getCAPublicKey();
                }

                if (caPublicKey.equals(certPublicKey)) {
                    return anchor;
                }

                if ("X.509".equals(caPublicKey.getFormat()) && "X.509".equals(certPublicKey.getFormat())) {
                    byte[] caPublicKeyEncoded = caPublicKey.getEncoded();
                    byte[] certPublicKeyEncoded = certPublicKey.getEncoded();
                    if (certPublicKeyEncoded != null && caPublicKeyEncoded != null && Arrays.equals(caPublicKeyEncoded, certPublicKeyEncoded)) {
                        return anchor;
                    }
                }
            } catch (Exception var9) {
            }
        }

        return null;
    }

    public Set<TrustAnchor> findAllByIssuerAndSignature(X509Certificate cert) {
        X500Principal issuer = cert.getIssuerX500Principal();
        synchronized (this.subjectToTrustAnchors) {
            List<TrustAnchor> anchors = (List) this.subjectToTrustAnchors.get(issuer);
            if (anchors == null) {
                return Collections.emptySet();
            } else {
                Set<TrustAnchor> result = new HashSet();
                Iterator var6 = anchors.iterator();

                while (var6.hasNext()) {
                    TrustAnchor anchor = (TrustAnchor) var6.next();

                    try {
                        X509Certificate caCert = anchor.getTrustedCert();
                        PublicKey publicKey;
                        if (caCert != null) {
                            publicKey = caCert.getPublicKey();
                        } else {
                            publicKey = anchor.getCAPublicKey();
                        }

                        if (publicKey != null) {
                            cert.verify(publicKey);
                            result.add(anchor);
                        }
                    } catch (Exception var11) {
                    }
                }

                return result;
            }
        }
    }
}

