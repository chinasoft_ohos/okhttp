package okhttp3.internal.platform.ohos;

import org.conscrypt.Conscrypt;

import javax.net.ssl.SSLSocket;

public class SSLSockets {
    private SSLSockets() {
    }

    public static boolean isSupportedSocket(SSLSocket socket) {
        return Conscrypt.isConscrypt(socket);
    }

    private static void checkSupported(SSLSocket s) {
        if (!isSupportedSocket(s)) {
            throw new IllegalArgumentException("Socket is not a supported socket.");
        }
    }

    public static void setUseSessionTickets(SSLSocket socket, boolean useSessionTickets) {
        checkSupported(socket);
        Conscrypt.setUseSessionTickets(socket, useSessionTickets);
    }
}
