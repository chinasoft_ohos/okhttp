package okhttp3.internal.platform;


import org.openjsse.net.ssl.OpenJSSE;

import javax.net.ssl.*;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class OpenJSSEPlatform extends Platform {
    private final Provider provider;
    private static final boolean isSupported;
    public static final OpenJSSEPlatform.Companion Companion = new OpenJSSEPlatform.Companion();

    public SSLContext newSSLContext() {
        SSLContext var10000 = null;
        try {
            var10000 = SSLContext.getInstance("TLSv1.3", this.provider);
        } catch (NoSuchAlgorithmException e) {
            e.fillInStackTrace();
        }
        return var10000;
    }

    public X509TrustManager platformTrustManager() {
        TrustManagerFactory factory = null;
        try {
            factory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm(), this.provider);
        } catch (NoSuchAlgorithmException e) {
            e.fillInStackTrace();
        }
        try {
            factory.init((KeyStore) null);
        } catch (KeyStoreException e) {
            e.fillInStackTrace();
        }
        TrustManager[] var10000 = factory.getTrustManagers();
        TrustManager[] trustManagers = var10000;
        boolean var3 = trustManagers.length == 1 && trustManagers[0] instanceof X509TrustManager;
        if (!var3) {
            StringBuilder var11 = (new StringBuilder()).append("Unexpected default trust managers: ");
            String var10001 = Arrays.toString(trustManagers);
            String var9 = var11.append(var10001).toString();
            throw new IllegalStateException(var9.toString());
        } else {
            TrustManager var10 = trustManagers[0];
            if (trustManagers[0] == null) {
                throw new NullPointerException("null cannot be cast to non-null type javax.net.ssl.X509TrustManager");
            } else {
                return (X509TrustManager) var10;
            }
        }
    }

    public X509TrustManager trustManager(SSLSocketFactory sslSocketFactory) {
        throw new UnsupportedOperationException("clientBuilder.sslSocketFactory(SSLSocketFactory) not supported with OpenJSSE");
    }

    public void configureTlsExtensions(SSLSocket sslSocket, String hostname, List protocols) {
        if (sslSocket instanceof SSLSocket) {
            SSLParameters sslParameters = (sslSocket).getSSLParameters();
            if (sslParameters instanceof SSLParameters) {
                List names = Platform.alpnProtocolNames(protocols);
                SSLParameters var10000 = sslParameters;
                Collection collection = (Collection) names;
                Object[] var10001 = collection.toArray(new String[0]);
                if (var10001 == null) {
                    throw new NullPointerException("null cannot be cast to non-null type java.Array<T>");
                }

                var10000.setProtocols((String[]) var10001);
                (sslSocket).setSSLParameters(sslParameters);
            }
        } else {
            try {
                super.configureTlsExtensions(sslSocket, hostname, protocols);
            } catch (IOException e) {
                e.fillInStackTrace();
            }
        }

    }

    public String getSelectedProtocol(SSLSocket sslSocket) {
        String var10000;
        if (sslSocket instanceof SSLSocket) {
            String protocol = sslSocket.getEnabledProtocols()[0];
            if (protocol != null) {
                switch (protocol.hashCode()) {
                    case 0:
                        if (protocol.equals("")) {
                            break;
                        }
                    default:
                        var10000 = protocol;
                        return var10000;
                }
            }

            var10000 = null;
        } else {
            var10000 = super.getSelectedProtocol(sslSocket);
        }

        return var10000;
    }

    private OpenJSSEPlatform() {
        this.provider = new OpenJSSE();
    }

    static {
        boolean var0;
        try {
            Class.forName("org.openjsse.net.ssl.OpenJSSE", false, Companion.getClass().getClassLoader());
            var0 = true;
        } catch (ClassNotFoundException var2) {
            var0 = false;
        }

        isSupported = var0;
    }


    public static final class Companion {
        public final boolean isSupported() {
            return OpenJSSEPlatform.isSupported;
        }

        public final OpenJSSEPlatform buildIfSupported() {
            return ((OpenJSSEPlatform.Companion) this).isSupported() ? new OpenJSSEPlatform() : null;
        }

        private Companion() {
        }

    }
}
