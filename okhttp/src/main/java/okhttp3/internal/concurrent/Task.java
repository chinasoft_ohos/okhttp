package okhttp3.internal.concurrent;

public abstract class Task {
    private TaskQueue queue;
    private long nextExecuteNanoTime;
    private final String name;
    private final boolean cancelable;

    public TaskQueue getQueue() {
        return this.queue;
    }

    public void setQueue(TaskQueue var1) {
        this.queue = var1;
    }

    public long getNextExecuteNanoTime() {
        return this.nextExecuteNanoTime;
    }

    public final void setNextExecuteNanoTime(long var1) {
        this.nextExecuteNanoTime = var1;
    }

    public abstract long runOnce();

    public final void initQueue(TaskQueue queue) {
        if (this.queue != queue) {
            boolean var2 = this.queue == null;
            boolean var3 = false;
            boolean var4 = false;
            if (!var2) {
                String var6 = "task is in multiple queues";
                throw new IllegalStateException(var6.toString());
            } else {
                this.queue = queue;
            }
        }
    }

    public String toString() {
        return this.name;
    }

    public final String getName() {
        return this.name;
    }

    public final boolean getCancelable() {
        return this.cancelable;
    }

    public Task(String name, boolean cancelable) {
        super();
        this.name = name;
        this.cancelable = cancelable;
        this.nextExecuteNanoTime = -1L;
    }
}
