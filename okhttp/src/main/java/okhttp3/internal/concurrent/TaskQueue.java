package okhttp3.internal.concurrent;

import okhttp3.internal.Util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;

public class TaskQueue {
    private boolean shutdown;
    private Task activeTask;
    private final List futureTasks;
    private boolean cancelActiveTask;
    private final TaskRunner taskRunner;
    private final String name;

    public final boolean getShutdown() {
        return this.shutdown;
    }

    public final void setShutdown(boolean var1) {
        this.shutdown = var1;
    }

    public final Task getActiveTask() {
        return this.activeTask;
    }

    public final void setActiveTask(Task var1) {
        this.activeTask = var1;
    }

    public final List getFutureTasks() {
        return this.futureTasks;
    }

    public final boolean getCancelActiveTask() {
        return this.cancelActiveTask;
    }

    public final void setCancelActiveTask$okhttp_parent_okhttp_main(boolean var1) {
        this.cancelActiveTask = var1;
    }


    public final void schedule(Task task, long delayNanos) {
        TaskRunner var4 = this.taskRunner;
        boolean var5 = false;
        boolean var6 = false;
        synchronized (var4) {
        }

        try {
            if (!this.shutdown) {
                if (this.scheduleAndDecide$okhttp_parent_okhttp_main(task, delayNanos, false)) {
                    this.taskRunner.kickCoordinator$okhttp_parent_okhttp_main(this);
                }

                return;
            }

            boolean $i$f$taskLog;
            boolean var13;
            String var14;
            if (!task.getCancelable()) {
                $i$f$taskLog = false;
                if (TaskRunner.Companion.getLogger().isLoggable(Level.FINE)) {
                    var13 = false;
                    var14 = "schedule failed (queue is shutdown)";
                }

                throw new RejectedExecutionException();
            }

            $i$f$taskLog = false;
            if (TaskRunner.Companion.getLogger().isLoggable(Level.FINE)) {
                var13 = false;
                var14 = "schedule canceled (queue is shutdown)";
            }
        } finally {
            ;
        }

    }

    // $FF: synthetic method
    public static void schedule$default(TaskQueue var0, Task var1, long var2, int var4, Object var5) {
        if ((var4 & 2) != 0) {
            var2 = 0L;
        }

        var0.schedule(var1, var2);
    }

    public final void schedule(String name, long delayNanos) {

        this.schedule((Task) (new Task(name, false) {
            public long runOnce() {
                return 0L;
            }
        }), delayNanos);
    }

    // $FF: synthetic method
    public static void schedule(TaskQueue var0, final String name, long delayNanos, int var5, Object var6) {
        if ((var5 & 2) != 0) {
            delayNanos = 0L;
        }

        var0.schedule((Task) (new Task(name, false) {
            public long runOnce() {
                return 0L;
            }
        }), delayNanos);
    }

    public final void execute(String name, long delayNanos, boolean cancelable) {

        this.schedule((Task) (new Task(name, cancelable) {
            public long runOnce() {
//                block.invoke();
                return -1L;
            }
        }), delayNanos);
    }

    // $FF: synthetic method
    public static void execute(TaskQueue var0, final String name, long delayNanos, final boolean cancelable, final Class block, int var6, Object var7) {
        if ((var6 & 2) != 0) {
            delayNanos = 0L;
        }

        if ((var6 & 4) != 0) {
//            cancelable = true;
        }


        var0.schedule((Task) (new Task(name, cancelable) {
            public long runOnce() {
//                block.invoke();
                return -1L;
            }
        }), delayNanos);
    }

    public final CountDownLatch idleLatch() {
        TaskRunner var1 = this.taskRunner;
        boolean var2 = false;
        boolean var3 = false;
        synchronized (var1) {
        }

        CountDownLatch var10;
        try {
            if (this.activeTask == null && this.futureTasks.isEmpty()) {
                CountDownLatch var11 = new CountDownLatch(0);
                return var11;
            }

            Task existingTask = this.activeTask;
            if (!(existingTask instanceof TaskQueue.AwaitIdleTask)) {
                Iterator var6 = this.futureTasks.iterator();

                Task futureTask;
                do {
                    if (!var6.hasNext()) {
                        TaskQueue.AwaitIdleTask newTask = new TaskQueue.AwaitIdleTask();
                        if (this.scheduleAndDecide$okhttp_parent_okhttp_main((Task) newTask, 0L, false)) {
                            this.taskRunner.kickCoordinator$okhttp_parent_okhttp_main(this);
                        }

                        CountDownLatch var8 = newTask.getLatch();
                        return var8;
                    }

                    futureTask = (Task) var6.next();
                } while (!(futureTask instanceof TaskQueue.AwaitIdleTask));

                CountDownLatch var9 = ((TaskQueue.AwaitIdleTask) futureTask).getLatch();
                return var9;
            }

            var10 = ((TaskQueue.AwaitIdleTask) existingTask).getLatch();
        } finally {
            ;
        }

        return var10;
    }

    public final boolean scheduleAndDecide$okhttp_parent_okhttp_main(Task task, long delayNanos, boolean recurrence) {
        task.initQueue(this);
        long now = this.taskRunner.getBackend().nanoTime();
        long executeNanoTime = now + delayNanos;
        int existingIndex = this.futureTasks.indexOf(task);
        boolean $i$f$taskLog;
        boolean $i$f$indexOfFirst;
        String var20;
        if (existingIndex != -1) {
            if (task.getNextExecuteNanoTime() <= executeNanoTime) {
                $i$f$taskLog = false;
                if (TaskRunner.Companion.getLogger().isLoggable(Level.FINE)) {
                    $i$f$indexOfFirst = false;
                    var20 = "already scheduled";
                }

                return false;
            }

            this.futureTasks.remove(existingIndex);
        }

        task.setNextExecuteNanoTime(executeNanoTime);
        if (TaskRunner.Companion.getLogger().isLoggable(Level.FINE)) {
            $i$f$indexOfFirst = false;
            var20 = recurrence ? "run again after " + "scheduled after " : "";
        }

        List $this$indexOfFirst$iv = this.futureTasks;
        $i$f$indexOfFirst = false;
        int index$iv = 0;
        Iterator var14 = $this$indexOfFirst$iv.iterator();

        int var10000;
        while (true) {
            if (!var14.hasNext()) {
                var10000 = -1;
                break;
            }

            Object item$iv = var14.next();
            Task it = (Task) item$iv;
            if (it.getNextExecuteNanoTime() - now > delayNanos) {
                var10000 = index$iv;
                break;
            }

            ++index$iv;
        }

        int insertAt = var10000;
        if (insertAt == -1) {
            insertAt = this.futureTasks.size();
        }

        this.futureTasks.add(insertAt, task);
        return insertAt == 0;
    }

    public final void cancelAll() {
        if (true && Thread.holdsLock(this)) {
            StringBuilder var10002 = (new StringBuilder()).append("Thread ");
            Thread var10003 = Thread.currentThread();
            throw new AssertionError(var10002.append(var10003.getName()).append(" MUST NOT hold lock on ").append(this).toString());
        } else {
            TaskRunner var1 = this.taskRunner;
//            $i$f$assertThreadDoesntHoldLock = false;
            boolean var3 = false;
            synchronized (var1) {
//                int var4 = false;
                if (this.cancelAllAndDecide$okhttp_parent_okhttp_main()) {
                    this.taskRunner.kickCoordinator$okhttp_parent_okhttp_main(this);
                }

//                Unit var6 = Unit.INSTANCE;
            }
        }
    }

    public final void shutdown() {
        if (true && Thread.holdsLock(this)) {
            StringBuilder var10002 = (new StringBuilder()).append("Thread ");
            Thread var10003 = Thread.currentThread();
            throw new AssertionError(var10002.append(var10003.getName()).append(" MUST NOT hold lock on ").append(this).toString());
        } else {
            TaskRunner var1 = this.taskRunner;
//            $i$f$assertThreadDoesntHoldLock = false;
            boolean var3 = false;
            synchronized (var1) {
//                int var4 = false;
                this.shutdown = true;
                if (this.cancelAllAndDecide$okhttp_parent_okhttp_main()) {
                    this.taskRunner.kickCoordinator$okhttp_parent_okhttp_main(this);
                }

//                Unit var6 = Unit.INSTANCE;
            }
        }
    }

    public final boolean cancelAllAndDecide$okhttp_parent_okhttp_main() {
        if (this.activeTask != null) {
            Task var10000 = this.activeTask;
            if (var10000.getCancelable()) {
                this.cancelActiveTask = true;
            }
        }

        boolean tasksCanceled = false;
        int i = this.futureTasks.size() - 1;

        for (boolean var3 = false; i >= 0; --i) {
            if (((Task) this.futureTasks.get(i)).getCancelable()) {
                Task task$iv = (Task) this.futureTasks.get(i);
//                int $i$f$taskLog = false;
                if (TaskRunner.Companion.getLogger().isLoggable(Level.FINE)) {
//                    int var7 = false;
                    String var10 = "canceled";
//                    TaskLoggerKt.access$log(task$iv, this, var10);
                }

                tasksCanceled = true;
                this.futureTasks.remove(i);
            }
        }

        return tasksCanceled;
    }

    public String toString() {
        return this.name;
    }

    public final TaskRunner getTaskRunner() {
        return this.taskRunner;
    }

    public final String getName() {
        return this.name;
    }

    public TaskQueue(TaskRunner taskRunner, String name) {
        super();
        this.taskRunner = taskRunner;
        this.name = name;
        boolean var3 = false;
        this.futureTasks = (List) (new ArrayList());
    }


    private static final class AwaitIdleTask extends Task {
        private final CountDownLatch latch = new CountDownLatch(1);

        public final CountDownLatch getLatch() {
            return this.latch;
        }

        public long runOnce() {
            this.latch.countDown();
            return -1L;
        }

        public AwaitIdleTask() {
            super("okhttp" + " awaitIdle", false);
        }
    }
}
