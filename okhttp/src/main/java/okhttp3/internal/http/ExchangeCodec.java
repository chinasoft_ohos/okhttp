/*
 * Copyright (C) 2012 The Harmony Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package okhttp3.internal.http;

import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.connection.RealConnection;
import okio.Sink;
import okio.Source;

import javax.annotation.Nullable;
import java.io.IOException;

/**
 * Encodes HTTP requests and decodes HTTP responses.
 */
public interface ExchangeCodec {
    /**
     * The timeout to use while discarding a stream of input data. Since this is used for connection
     * reuse, this timeout should be significantly less than the time it takes to establish a new
     * connection.
     */
    int DISCARD_STREAM_TIMEOUT_MILLIS = 100;

    RealConnection connection();

    Sink createRequestBody(Request request, long contentLength) throws IOException;

    void writeRequestHeaders(Request request) throws IOException;

    void flushRequest() throws IOException;

    void finishRequest() throws IOException;

    @Nullable
    Response.Builder readResponseHeaders(boolean expectContinue) throws IOException;

    long reportedContentLength(Response response) throws IOException;

    Source openResponseBodySource(Response response) throws IOException;

    Headers trailers() throws IOException;

    void cancel();
}
