/*
 * Copyright (C) 2017 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package okhttp3;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;

/**
 * Listener for metrics events. Extend this class to monitor the quantity, size, and duration of
 * your application's HTTP calls.
 *
 * <p>All start/connect/acquire events will eventually receive a matching end/release event,
 * either successful (non-null parameters), or failed (non-null throwable).  The first common
 * parameters of each event pair are used to link the event in case of concurrent or repeated
 * events e.g. dnsStart(call, domainName) -&gt; dnsEnd(call, domainName, inetAddressList).
 *
 * <p>Nesting is as follows
 * <ul>
 *   <li>call -&gt; (dns -&gt; connect -&gt; secure connect)* -&gt; request events</li>
 *   <li>call -&gt; (connection acquire/release)*</li>
 * </ul>
 *
 * <p>Request events are ordered:
 * requestHeaders -&gt; requestBody -&gt; responseHeaders -&gt; responseBody
 *
 * <p>Since connections may be reused, the dns and connect events may not be present for a call,
 * or may be repeated in case of failure retries, even concurrently in case of happy eyeballs type
 * scenarios. A redirect cross domain, or to use https may cause additional connection and request
 * events.
 *
 * <p>All event methods must execute fast, without external locking, cannot throw exceptions,
 * attempt to mutate the event parameters, or be re-entrant back into the client.
 * Any IO - writing to files or network should be done asynchronously.
 */
public abstract class EventListener {
  public static final EventListener NONE = new EventListener() {
  };

  static Factory factory(EventListener listener) {
    return call -> listener;
  }

  public void callStart(Call call) {
  }

  /**
   * Invoked just prior to a DNS lookup. See {@link Dns#lookup(String)}.
   *
   * <p>This can be invoked more than 1 time for a single {@link Call}. For example, if the response
   * to the {@link Call#request()} is a redirect to a different host.
   *
   * <p>If the {@link Call} is able to reuse an existing pooled connection, this method will not be
   * invoked. See {@link ConnectionPool}.
   *
   * @param call call
   * @param domainName domainName
   */
  public void dnsStart(Call call, String domainName) {
  }

  /**
   * Invoked immediately after a DNS lookup.
   *
   * <p>This method is invoked after {@link #dnsStart}.
   *
   * @param call call
   * @param domainName domainName
   * @param inetAddressList inetAddressList
   */
  public void dnsEnd(Call call, String domainName, List<InetAddress> inetAddressList) {
  }


  public void connectStart(Call call, InetSocketAddress inetSocketAddress, Proxy proxy) {
  }


  public void secureConnectStart(Call call) {
  }


  public void secureConnectEnd(Call call, @Nullable Handshake handshake) {
  }

  /**
   * Invoked immediately after a socket connection was attempted.
   *
   * <p>If the {@code call} uses HTTPS, this will be invoked after
   * {@link #secureConnectEnd(Call, Handshake)}, otherwise it will invoked after
   * {@link #connectStart(Call, InetSocketAddress, Proxy)}.
   *
   * @param call call
   * @param inetSocketAddress inetSocketAddress
   * @param proxy proxy
   * @param protocol protocol
   */
  public void connectEnd(Call call, InetSocketAddress inetSocketAddress, Proxy proxy,
      @Nullable Protocol protocol) {
  }

  /**
   * Invoked when a connection attempt fails. This failure is not terminal if further routes are
   * available and failure recovery is enabled.
   *
   * <p>If the {@code call} uses HTTPS, this will be invoked after {@link #secureConnectEnd(Call,
   * Handshake)}, otherwise it will invoked after {@link #connectStart(Call, InetSocketAddress,
   * Proxy)}.
   *
   * @param call call
   * @param inetSocketAddress inetSocketAddress
   * @param proxy proxy
   * @param protocol protocol
   * @param ioe ioe
   */
  public void connectFailed(Call call, InetSocketAddress inetSocketAddress, Proxy proxy,
      @Nullable Protocol protocol, IOException ioe) {
  }

  /**
   * Invoked after a connection has been acquired for the {@code call}.
   *
   * <p>This can be invoked more than 1 time for a single {@link Call}. For example, if the response
   * to the {@link Call#request()} is a redirect to a different address.
   *
   * @param call call
   * @param connection connection
   */
  public void connectionAcquired(Call call, Connection connection) {
  }

  /**
   * Invoked after a connection has been released for the {@code call}.
   *
   * <p>This method is always invoked after {@link #connectionAcquired(Call, Connection)}.
   *
   * <p>This can be invoked more than 1 time for a single {@link Call}. For example, if the response
   * to the {@link Call#request()} is a redirect to a different address.
   *
   * @param call call
   * @param connection connection
   */
  public void connectionReleased(Call call, Connection connection) {
  }

  public void requestHeadersStart(Call call) {
  }

  public void requestHeadersEnd(Call call, Request request) {
  }

  public void requestBodyStart(Call call) {
  }

  public void requestBodyEnd(Call call, long byteCount) {
  }

  public void requestFailed(Call call, IOException ioe) {
  }


  public void responseHeadersStart(Call call) {
  }

  public void responseHeadersEnd(Call call, Response response) {
  }

  public void responseBodyStart(Call call) {
  }

  public void responseBodyEnd(Call call, long byteCount) {
  }

  public void responseFailed(Call call, IOException ioe) {
  }

  public void callEnd(Call call) {
  }

  public void callFailed(Call call, IOException ioe) {
  }

  public interface Factory {
    /**
     * Creates an instance of the {@link EventListener} for a particular {@link Call}. The returned
     * {@link EventListener} instance will be used during the lifecycle of the {@code call}.
     *
     * <p>This method is invoked after the {@code call} is created. See
     * {@link OkHttpClient#newCall(Request)}.
     *
     * <p><strong>It is an error for implementations to issue any mutating operations on the
     * {@code call} instance from this method.</strong>
     *
     * @param call call
     * @return EventListener EventListener
     */
    EventListener create(Call call);
  }
}
