/*
 * Copyright (C) 2018 Square, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package okhttp3.sse;

import javax.annotation.Nullable;
import okhttp3.Response;

public abstract class EventSourceListener {

  public void onOpen(EventSource eventSource, Response response) {
  }

  public void onEvent(EventSource eventSource, @Nullable String id, @Nullable String type,
      String data) {
  }

  public void onClosed(EventSource eventSource) {
  }

  public void onFailure(EventSource eventSource, @Nullable Throwable t,
      @Nullable Response response) {
  }
}
