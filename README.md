# okhttp

#### 项目介绍

- 项目名称：okhttp
- 所属系列：openharmony的第三方组件适配移植
- 功能：PUT，DELETE，POST，GET等请求、文件的上传下载、加载图片(内部会图片大小自动压缩)、支持请求回调，直接返回对象、对象集合、支持session的保持
- 基线版本：Release v4.9.1
- 项目移植状态：有差异
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 编程语言：Java
- 外部库依赖：gradle依赖com.squareup.okio:okio-1.17.3

#### 安装教程

1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2.在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:okhttp:1.0.1')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


#### 代码演示
- 这个程序下载一个URL并以字符串的形式输出它的内容。
```
public class ResponseBody{
OkHttpClient client = new OkHttpClient();

String run(String url) throws IOException {
  Request request = new Request.Builder()
      .url(url)
      .build();

  try (Response response = client.newCall(request).execute()) {
    return response.body().string();
  }
}
}
```
- 这个程序向服务发送数据。
```
public class ResponseBody{
public static final MediaType JSON
    = MediaType.get("application/json; charset=utf-8");

OkHttpClient client = new OkHttpClient();

String post(String url, String json) throws IOException {
  RequestBody body = RequestBody.create(JSON, json);
  Request request = new Request.Builder()
      .url(url)
      .post(body)
      .build();
  try (Response response = client.newCall(request).execute()) {
    return response.body().string();
  }
}
}
```
#### 使用说明

- 1.okhttp支持ohos 2.0+ 和Java 8+运行。
- 2.okhttp依赖于高性能I/O。两者都是小型库，具有很强的向后兼容性。
- 3.我们强烈建议您保持okhttp的最新。与自动更新web浏览器一样，保持最新代码。
- 4.使用HTTPS客户端是防范潜在安全问题的重要手段
- 5.动态TLS生态系统和调整okhttp以改善连接和安全。
- 6.okhttp使用平台内置的TLS实现。在Java平台上，OkHttp也支持Conscrypt，它集成了BoringSSL和Java。如果是，OkHttp将使用Conscrypt
- 7.查看okcurl模块中的构建示例。

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.1

#### 版权和许可信息

```
Copyright 2019 Square, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
